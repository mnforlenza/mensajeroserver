import unittest
from test_user_services import TestUserServices
from test_message_services import TestMessageServices

if __name__ == '__main__':
	suite = unittest.TestLoader().discover(".", pattern='test_**services.py', top_level_dir=None)
	unittest.TextTestRunner().run(suite)
