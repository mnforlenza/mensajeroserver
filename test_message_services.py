import requests
import unittest
import json

class TestMessageServices(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestMessageServices, self).__init__(*args, **kwargs)
		self.__api_base_url = "http://localhost:8000"
		self.__base_url = "/api"
		self.__user_url = "/msg"
		request_body = { 'userName' : 'testmsg1', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		request_body = { 'userName' : 'testmsg2', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		request_body = { 'userFrom' : 'testmsg1', 'userTo': 'testmsg2', 'content' : 'hola', 'typeOf' : 'text' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		
	def test_message_create(self):
		request_body = { 'userFrom' : 'testmsg1', 'userTo': 'testmsg2', 'content' : 'hola', 'typeOf' : 'text' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
		
	def test_message_all(self):
		request_body = { 'userFrom' : 'testmsg1' , 'userTo' : 'testmsg2'}
		user = '/all'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params=request_body)
		self.assertEqual(r.status_code, 200)
	
	def test_message_all_should_find(self):
		request_body = { 'userFrom' : 'testmsg1' , 'userTo' : 'testmsg2'}
		user = '/all'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params=request_body)
		data_string = json.dumps(r.json())
		self.assertEqual('hola' in data_string, True)
		
	def test_message_last(self):
		request_body = { 'userName' : 'testmsg1'}
		user = '/last'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params=request_body)
		self.assertEqual(r.status_code, 200)
	
	def test_message_last_should_find(self):
		request_body = { 'userName' : 'testmsg1'}
		user = '/last'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params=request_body)
		data_string = json.dumps(r.json())
		self.assertEqual('hola' in data_string, True)
		
		
