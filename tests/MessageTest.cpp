#include <iostream>
#include "gtest/gtest.h"

#include "JsonUtil.h"
#include "Message.h"
#include "UserServiceImpl.h"
#include "User.h"
#include "ServiceProvider.h"
#include "UserServiceImpl.h"
#include "User.h"
#include "ServiceProvider.h"
#include "MessageService.h"


using namespace std;

TEST(MessageTest, MessageToJson) {
	Message message("user1", "user2","Hola, Como estas?","text","10/03/2015");
	string json = message.toJson();
	EXPECT_EQ(json,
			"{\"userFrom\":\"user1\",\"userTo\":\"user2\",\"content\":\"Hola, Como estas?\",\"date\":\"10/03/2015\",\"typeOf\":\"text\"}");
}

TEST(MessageTest, MessageCreateShouldWork) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	MessageService* messageService = ServiceProvider::getMessageService();

	try {
		User resultUser = userService->create("testmsg1", "1234",
				"test@gmail.com");
	} catch (UserNameUnavailableException e) {}

	try {
		User resultUser = userService->create("testmsg2", "1234",
				"test@gmail.com");
	} catch (UserNameUnavailableException e) {}

	messageService->create("testmsg1", "testmsg2", "hola", "text","10/03/2015");
	string result = messageService->getMessages("testmsg1","testmsg2");
	EXPECT_EQ( result.find("\"content\":\"hola\"") != std::string::npos, true);
}

TEST(MessageTest, LastMessageShouldWork) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	MessageService* messageService = ServiceProvider::getMessageService();

	try {
		User resultUser = userService->create("testmsg1", "1234",
				"test@gmail.com");
	} catch (UserNameUnavailableException e) {}

	try {
		User resultUser = userService->create("testmsg2", "1234",
				"test@gmail.com");
	} catch (UserNameUnavailableException e) {}

	messageService->create("testmsg1", "testmsg2", "hola", "text","10/03/2015");
	string result = messageService->getLastMessages("testmsg1");
	EXPECT_EQ( result.find("\"content\":\"hola\"") != std::string::npos, true);
}
