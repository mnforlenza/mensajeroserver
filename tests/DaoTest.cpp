#include <iostream>
#include "gtest/gtest.h"

#include "ConfigurationBuilder.h"
#include "Configuration.h"
#include "DbManager.h"

using namespace std;

TEST(DaoTest, DbManager) {
	ConfigurationBuilder builder("./conf/conf.json");
	Configuration conf = builder.build();

	DbManager* instance = DbManager::getInstance(conf);

	instance->put("test","ok");
	string value;
	instance->get("test",value);
	EXPECT_EQ(value, "ok");
}
