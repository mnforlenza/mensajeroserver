#include <iostream>
#include "gtest/gtest.h"

#include <json/json.h>
#include "JsonUtil.h"
#include "UserNotFoundException.h"


using namespace std;

TEST(UtilsTest, TestToJsonGeneric) {
	string json = "{\"example\":\"ex\"}";
	Json::Value value = JsonUtil::getJsonFromString(json);
	const Json::Value result = value["example"];
	EXPECT_EQ(result.asString(),"ex");
}

//Only use to test printing, not a real test
TEST(UtilsTest, PrintJson) {
	string json = "{\"example\":\"ex\"}";
	Json::Value value = JsonUtil::getJsonFromString(json);
	JsonUtil::printJson(value);
	EXPECT_EQ(1,1);
}

TEST(UtilsTest, ParseUserMustFail) {
	try {
		string json = "notajson";
		User user = JsonUtil::userFromJson(json);
		EXPECT_EQ(1, 0);
	} catch (UserNotFoundException e) {
		EXPECT_EQ(1, 1);
	}

}
