#include <iostream>
#include "gtest/gtest.h"

#include "JsonUtil.h"
#include "ConfigurationBuilder.h"
#include "Configuration.h"
#include "DaoUser.h"
#include "UserServiceImpl.h"
#include "User.h"
#include "ServiceProvider.h"
#include "UserNotFoundException.h"
#include "WrongPasswordException.h"
#include "UserNameUnavailableException.h"

using namespace std;

TEST(UserTest, UserToJson) {
	User user("testuser1", "password");
	user.setEmail("test@gmail.com");
	user.setOnline();
	string json = user.toJson();
	EXPECT_EQ(json,
			"{\"userName\":\"testuser1\",\"password\":\"password\",\"email\":\"test@gmail.com\",\"photo\":\"\",\"location\":\"\",\"locationPlace\":\"\",\"locationPlaceDescription\":\"\",\"active\":\"false\",\"online\":\"true\"}");
}

TEST(UserTest, JsonToUser) {
	User user =
			JsonUtil::userFromJson(
					"{\"userName\":\"testuser1\",\"password\":\"password\",\"email\":\"test@gmail.com\",\"active\":\"false\",\"online\":\"true\"}");
	EXPECT_EQ(user.getUserName(), "testuser1");
	EXPECT_EQ(user.getEmail(), "test@gmail.com");
	EXPECT_EQ(user.getPassword(), "password");
}

TEST(UserTest, CreateServiceInitial) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	try {
		User resultUser = userService->create("testuser1", "1234",
				"test@gmail.com");
		EXPECT_EQ(resultUser.getUserName(), "testuser1");
	} catch (UserNameUnavailableException e) {
		EXPECT_EQ(1, 1);
	}
	userService->activeUser("testuser1");
}

TEST(UserTest, CreateServiceFailed) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		User resultUser = userService->create("testuser1", "nanana", "nanana");
		EXPECT_EQ(1, 0);
	} catch (UserNameUnavailableException e) {
		EXPECT_EQ(1, 1);
	}
}

TEST(UserTest, GetUserService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		User resultUser = userService->getUser("testuser1");
		EXPECT_EQ(resultUser.getUserName(), "testuser1");
	} catch (UserNameUnavailableException e) {
		EXPECT_EQ(1, 0);
	}
}

TEST(UserTest, GetUserServiceFailed) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		User resultUser = userService->getUser("cualquiera");
		EXPECT_EQ(1, 0);
	} catch (UserNotFoundException e) {
		EXPECT_EQ(1, 1);
	}
}

TEST(UserTest, RemoveUserService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	userService->removeUser("testuser1");
	EXPECT_EQ(1, 1);
}

TEST(UserTest, CreateService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	User resultUser = userService->create("testuser1", "1234",
			"test@gmail.com");
	try {
		EXPECT_EQ(resultUser.getUserName(), "testuser1");
	} catch (UserNameUnavailableException e) {
		EXPECT_EQ(1, 0);
	}
}

TEST(UserTest, UserOnlineService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	EXPECT_EQ(userService->isUserOnline("testuser1"), false);
}

TEST(UserTest, UserOfflineService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	userService->logout("testuser1");
	EXPECT_EQ(userService->isUserOnline("testuser1"), false);
}

TEST(UserTest, UserLoginService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		userService->login("testuser1", "1234");
		EXPECT_EQ(1, 1);
	} catch (WrongPasswordException e) {
		EXPECT_EQ(1, 0);
	}
}

TEST(UserTest, UserWrongLoginService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		userService->login("testuser1", "12345");
		EXPECT_EQ(1, 0);
	} catch (WrongPasswordException e) {
		EXPECT_EQ(1, 1);
	}
}

TEST(UserTest, UserNotFoundLoginService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	try {
		userService->login("sdfsdfsdf", "12345");
		EXPECT_EQ(1, 0);
	} catch (UserNotFoundException e) {
		EXPECT_EQ(1, 1);
	}
}

TEST(UserTest, UpdateLocationService) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	try {
		User resultUser = userService->create("locationUser", "1234",
			"test@gmail.com");
	} catch (UserNameUnavailableException e) {

	}
	userService->updateLocation("locationUser", "-34.617480", "-58.372276");
	User returnUser = userService->getUser("locationUser");
	EXPECT_EQ(returnUser.getLocation(),"-34.617480:-58.372276");
	EXPECT_EQ(returnUser.getLocationPlace(),"-34.617525:-58.368285");
	EXPECT_EQ(returnUser.getLocationPlaceDescription(),"Facultad Ingenieria UBA, Paseo Colón");
}


TEST(UserTest, ListUsers) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	try {
		User resultUser = userService->create("locationUser", "1234",
			"test@gmail.com");
	} catch (UserNameUnavailableException e) {

	}
	string userName = "locationUser";
	string result = userService->getUsers();
	bool found = false;
	if (result.find(userName) != std::string::npos) {
	    found = true;
	}
	EXPECT_EQ(found, true);
}

TEST(UserTest, Update) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	try {
		User resultUser = userService->create("locationUser", "1234",
			"test@gmail.com");
	} catch (UserNameUnavailableException e) {

	}
	userService->update("locationUser", "mnforlenza@gmail.com", "photo1");
	User resultUser = userService->getUser("locationUser");
	EXPECT_EQ(resultUser.getEmail(), "mnforlenza@gmail.com");
}

TEST(UserTest, SetOnline) {
	UserServiceImpl* userService = ServiceProvider::getUserService();

	try {
		User resultUser = userService->create("locationUser", "1234",
			"test@gmail.com");
	} catch (UserNameUnavailableException e) {

	}
	userService->setOnline("locationUser");

	EXPECT_EQ(userService->isUserOnline("locationUser"), true);
}
