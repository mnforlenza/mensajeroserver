#include <iostream>
#include "gtest/gtest.h"

#include "Logger.h"
#include "ConfigurationBuilder.h"
#include "Configuration.h"
#include "ConfigurationException.h"

using namespace std;

inline bool exists (const std::string& name) {
    ifstream f(name.c_str());
    if (f.good()) {
        f.close();
        return true;
    } else {
        f.close();
        return false;
    }
}

TEST(LoggerTest, Log) {
	Logger* logger = Logger::getLogger("LoggerTest");
	logger->info("Testing logger");
	logger->debug("Testing logger");
	logger->warn("Testing logger");
	logger->error("Testing logger");
	logger->fatal("Testing logger");
	delete logger;
}

TEST(LoggerTest, LogByLevel) {
	ConfigurationBuilder builder("./conf/conf.json");
	Configuration result = builder.build();
	Logger* logger = Logger::getLogger("LoggerTest");
	logger->info("Testing logger 2");
	delete logger;
	EXPECT_EQ(exists(result.getLoggerName()),true);
}

