#include <iostream>
#include "gtest/gtest.h"

#include "Place.h"
#include "InstancePlaces.h"

using namespace std;

TEST(PlacesTest, CheckLoading) {
	InstancePlaces* instance = InstancePlaces::getInstance();
	EXPECT_EQ(instance->getPlacesCount()>0,true);
}

TEST(PlacesTest, CalculateZeroDistance) {
	Place firstPlace = Place("-34.617525", "-58.368285", "Facultad Ingenieria UBA, Paseo Colón");
	EXPECT_EQ(firstPlace.calculateDistance(firstPlace),0);
}

TEST(PlacesTest, CalculateDistance) {
	Place firstPlace = Place("-34.617525", "-58.368285", "Facultad Ingenieria UBA, Paseo Colón");
	Place secondPlace = Place("-34.588353", "-58.396256", "Facultad Ingenieria UBA, Las Heras ");
	double result = firstPlace.calculateDistance(secondPlace);
	EXPECT_EQ(result>0.0404151 && result < 0.040416, true);
}

TEST(PlacesTest, CalculateNearest1) {
	InstancePlaces* instance = InstancePlaces::getInstance();
	Place nearest = instance->getNearestPlace("-34.617480", "-58.372276");
	EXPECT_EQ(nearest.getDescription(),"Facultad Ingenieria UBA, Paseo Colón");
}

TEST(PlacesTest, CalculateNearest2) {
	InstancePlaces* instance = InstancePlaces::getInstance();
	Place nearest = instance->getNearestPlace("-34.587558", "-58.393461");
	EXPECT_EQ(nearest.getDescription(),"Facultad Ingenieria UBA, Las Heras");
}

TEST(PlacesTest, CalculateNearest3) {
	InstancePlaces* instance = InstancePlaces::getInstance();
	Place nearest = instance->getNearestPlace("-34.545143", "-58.449840");
	EXPECT_EQ(nearest.getDescription(),"Ciudad Universitaria UBA");
}

