#include <iostream>
#include "gtest/gtest.h"

#include "ConfigurationBuilder.h"
#include "Configuration.h"

using namespace std;

TEST(ConfigurationTest, CheckBasicConfiguration) {
	ConfigurationBuilder builder("./conf/conf.json");
	Configuration result = builder.build();

	EXPECT_EQ(result.getLoggerLevel().length()>0,true);
	EXPECT_EQ(result.getServerPort().length()>0,true);
	EXPECT_EQ(result.getProdDbDirectory().length()>0,true);
	EXPECT_EQ(result.getTestDbDirectory().length()>0,true);
}

TEST(ConfigurationTest, CheckDbDirectory) {
	ConfigurationBuilder builder("./conf/conf.json");
	Configuration result = builder.build();
	if(result.isTestEnabled()){
		EXPECT_EQ(result.getDbDirectory(), result.getTestDbDirectory());
	}else{
		EXPECT_EQ(result.getDbDirectory(), result.getProdDbDirectory());
	}
}

