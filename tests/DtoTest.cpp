#include <iostream>
#include "gtest/gtest.h"
#include "json/json.h"

#include "GenericResponseDto.h"
#include "UserNotFoundException.h"

using namespace std;

TEST(DtoTest, ShouldWorksTest) {
	GenericResponseDto dto("user","{\"userName\":\"testuser1\",\"password\":\"password\",\"email\":\"test@gmail.com\",\"active\":\"false\",\"online\":\"true\"}");
	string result = dto.toJson();

	cout<<result<<endl;

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(result, root, false);

	const Json::Value onlineJson = root["succes"];
	EXPECT_EQ(onlineJson.asString(),"true");
}

TEST(DtoTest, ShouldFailedTest) {
	UserNotFoundException e;
	GenericResponseDto dto(e);
	string result = dto.toJson();

	cout<<result<<endl;

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(result, root, false);

	const Json::Value onlineJson = root["succes"];
	EXPECT_EQ(onlineJson.asString(),"false");
}
