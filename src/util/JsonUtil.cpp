/*
 * JsonUtil.cpp
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#include "JsonUtil.h"

JsonUtil::JsonUtil() {
	// TODO Auto-generated constructor stub

}

JsonUtil::~JsonUtil() {
	// TODO Auto-generated destructor stub
}

/**
 * Transforma a json cualquier variable booleana
 * @param name el nombre del root de json
 * @param boolean el booleano
 * @return un string con la representación en json
 */ 
string JsonUtil::toJson(string name, bool boolean){
	if(boolean){
		return "\"" + name + "\":" + "\"true\"";
	}
	return "\"" + name + "\":" + "\"false\"";
}

/**
 * Lee un archivo de configuración json
 * @param filename el nombre de archivo
 * @return la representación jsoncpp del json
 */ 
Json::Value JsonUtil::getJsonFromFile(string filename){
	string line;
	string result = "";
	ifstream myfile(filename);

	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			result += line;
		}
		myfile.close();
	}

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(result, root, false);
	return root;
}

/**
 * Transforma a json desde un string
 * @param data el string
 * @return un string con la representación en json
 */ 
Json::Value JsonUtil::getJsonFromString(string data)
{
	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(data, root, false);

	return root;
}

/**
 * Imprime el json por consola
 * @param el valor de json por jsoncpp
 */ 
void JsonUtil::printJson(Json::Value jsonvalue){
	Json::StyledWriter writer;
	printf("%s\n", writer.write(jsonvalue).c_str());
}

string JsonUtil::toString(Json::Value jsonvalue){

cout << jsonvalue;

	return jsonvalue.asString();
}

/**
 * Obtiene un usuario desde una represenación json
 * @param json la representación json del usuario
 * @return el objeto usuario 
 */ 
User JsonUtil::userFromJson(string json) {
	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(json, root, false);

	if (!parsedSuccess) {
		throw UserNotFoundException();
	}


	const Json::Value userNameJson = root["userName"];
	const Json::Value passwordJson = root["password"];
	const Json::Value emailJson = root["email"];
	const Json::Value photoJson = root["photo"];
	const Json::Value onlineJson = root["online"];
	const Json::Value activeJson = root["active"];
	const Json::Value locationJson = root["location"];
	const Json::Value locationPlaceJson = root["locationPlace"];
	const Json::Value locationPlaceDescJson = root["locationPlaceDescription"];

	User result(userNameJson.asString(),passwordJson.asString());
	//result.setEmail(emailJson.asString());
	result.update(emailJson.asString(),photoJson.asString());
	result.updateLocation(locationJson.asString(),locationPlaceJson.asString(), locationPlaceDescJson.asString());
	if (onlineJson.asString() == "true") {
			result.setOnline();
		} else {
			result.setOffline();
	}
	if (activeJson.asString() == "true") {
				result.activeUser();
			} else {
				result.deactiveUser();
		}
	return result;
}
