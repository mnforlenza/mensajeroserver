/*
 * JsonUtil.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef JSONUTIL_H_
#define JSONUTIL_H_

#ifndef USER_H_
#include "User.h"
#endif

#ifndef SRC_EXCEPTION_USERNOTFOUNDEXCEPTION_H_
#include "UserNotFoundException.h"
#endif

#include <fstream>
#include <iostream>
#include <json/json.h>

using namespace std;

class User;

/**
 * Clase utilitaria para serializar y deserializar 
 */
class JsonUtil {
public:
	JsonUtil();
	static string toJson(string name, bool boolean);
	static Json::Value getJsonFromFile(string filename);
	static Json::Value getJsonFromString(string data);
	static void printJson(Json::Value jsonvalue);
	static string toString(Json::Value jsonvalue);
	static User userFromJson(string json);

	~JsonUtil();
};

#endif /* JSONUTIL_H_ */
