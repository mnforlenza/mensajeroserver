#include "ControllerFactory.h"


ControllerFactory::ControllerFactory()
{
	configuracionInicial();
}

void ControllerFactory::configuracionInicial(){
	this->listadoControllers = JsonUtil::getJsonFromFile("./conf/controllerfactory.json");
}

void ControllerFactory::getControllersInstances(vector<Controller *> &controllers){

	LoginController * loginController = new LoginController(this->listadoControllers["login"]);
	controllers.push_back(loginController);

	MessageController * msgController = new MessageController(this->listadoControllers["msg"]);
	controllers.push_back(msgController);

	UserController * userController = new UserController(this->listadoControllers["user"]);
	controllers.push_back(userController);

}

ControllerFactory::~ControllerFactory()
{

}