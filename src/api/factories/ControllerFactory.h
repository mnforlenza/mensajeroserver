/*
 * Factory para controllers desde json
 */

#ifndef _CONTROLLER_FACTORY_H
#define _CONTROLLER_FACTORY_H

#include "Controller.h"
#include "LoginController.h"
#include "UserController.h"
#include "MessageController.h"
#include "JsonUtil.h"

class ControllerFactory
{
	public:
		ControllerFactory();
		void getControllersInstances(vector<Controller *>  &controllers);
		~ControllerFactory();

	private:
		void configuracionInicial();
		Json::Value listadoControllers;
};

#endif /* _CONTROLLER_FACTORY_H */
