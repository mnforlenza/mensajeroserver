/*
 *
 */

#ifndef _SERVER_H
#define _SERVER_H

#include "Controller.h"
#include "ControllerFactory.h"
#include "Configuration.h"
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "mongoose.h"
#include "Request.h"
#include "Logger.h"


using namespace std;

class Server
{
	public:
		Server(Configuration& configuration);
		~Server();
		bool handleRequest(Request *req);
		void start();
		void stop();	

	private:
	   	static int event_handle(struct mg_connection *conn, enum mg_event ev);
		struct mg_server *server;
		::Logger* logger;
		string port;
		vector<Controller *> controllers;
		ControllerFactory* factory;
		Configuration* configuration;

};

#endif /* _SERVER_H */
