#include <cstring>
#include <unistd.h>
#include "Server.h"
#include "Route.h"
#include "LoginController.h"
#include "ConfigurationBuilder.h"
#include "Configuration.h"
#include "DbManager.h"


#include <iostream>
using namespace std;

int main(void) 
{

	ConfigurationBuilder builder("./conf/conf.json");
	Configuration result = builder.build();

	cout << result.getLoggerLevel()<<endl;
	cout << result.getServerPort()<<endl;
	cout << result.getProdDbDirectory()<<endl;
	cout << result.getTestDbDirectory()<<endl;

	DbManager* dbManager;
	dbManager = DbManager::getInstance(result);

	Server server(result);

	server.start();

	return 0;
}
