#ifndef _ROUTES_H
#define _ROUTES_H

#include <map>
#include <iostream>
#include <string>
#include "Request.h"



using namespace std;


class Route
{

	public:
		string url;
		string httpMethod;
		typedef void (*callback_function)(Request * req); 
		callback_function function;

	public:

		Route(string httpMethod,string url,callback_function function);
        ~Route();
        //void addRoute(string httpMethod,string url,callback_function Controller);

};


#endif /* _ROUTES_H */
