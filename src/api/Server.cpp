#include "Server.h"


Server::Server(Configuration& configuration)
{
	port = configuration.getServerPort();

  factory = new ControllerFactory();
  factory->getControllersInstances(controllers);
  logger = ::Logger::getLogger("Server.cpp");
}

void Server::start()
{

	server = mg_create_server(this,this->event_handle);

	mg_set_option(server, "listening_port", port.c_str());
	logger->info(string("Starting on port") + mg_get_option(server, "listening_port"));
	while(1) 
	{
		mg_poll_server(server, 1000);
	}

}

bool Server::handleRequest(Request *req)
{
  stringstream url(req->getUri());


  string controller;
  getline(url, controller, '/');
  getline(url, controller, '/');
  if(controller=="api")
    getline(url, controller, '/');
  else
    return false;

  vector<Controller *>::iterator it;
  for (it=controllers.begin(); it!=controllers.end(); it++) 
  {
                  cout << (*it)->rootPath<<endl;

    if (controller == (*it)->rootPath)
    {
      (*it)->handle(req);
      return true;
    }

  }
  return false;
}

int Server::event_handle(struct mg_connection *conn, enum mg_event ev)
{

    const char *s_no_cache_header =
  "Cache-Control: max-age=0, post-check=0, "
  "pre-check=0, no-store, no-cache, must-revalidate\r\n";


  switch (ev) {
    case MG_AUTH: return MG_TRUE;
    case MG_REQUEST:
    {
      Server * server = (Server *)conn->server_param;
      
      Request request(conn);

      if (server->handleRequest(&request))
      {
        return MG_TRUE;          
      }

      mg_send_file(conn, "src/api/index.html", s_no_cache_header);
      return MG_MORE;
    }
    default: return MG_FALSE;

  }

}


void Server::stop()
{
	mg_destroy_server(&server);
}


Server::~Server()
{
	stop();
  
  for ( size_t i = 0; i < controllers.size(); i++)
  {
      delete controllers[i];
  }
  delete logger;


}
