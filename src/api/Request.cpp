#include "Request.h"


Request::Request(struct mg_connection *conn)
{
	this->conn = conn;
	this->uri = string(conn->uri);
	this->method = string(conn->request_method);
	if (conn->query_string!=NULL)
		this->params = string(conn->query_string);

	ostringstream post;
    post.write(conn->content, conn->content_len);
    this->body = post.str();
    this->jsonData =  JsonUtil::getJsonFromString(this->body);
}

string Request::getParamFromUri(string param)
{
	char buffer[100];

 	mg_get_var(this->conn,param.c_str(), buffer, sizeof(buffer));

 	return string(buffer);

}


string Request::getParamFromBody(string param)
{

 	return this->jsonData[param].asString();

}


void Request::setResponseJson(string param)
{

	Json::Value fromScratch;
	fromScratch["result"] = param;
	Json::StyledWriter writer;
    mg_printf_data(this->conn, "%s",writer.write(fromScratch).c_str());

}

void Request::setRawResponse(string json)
{
    mg_printf_data(this->conn, "%s",json.c_str());

}

string Request::getUri()
{
	return this->uri;
}

string Request::getMethod()
{
	return this->method;
}

void Request::printData()
{
	cout << "URL " <<this->uri <<endl;
	cout << "Metodo " <<this->method <<endl;
	cout << "Parametros " <<this->params <<endl;
	cout << "Body " <<this->body <<endl;

}



Request::~Request()
{

}
