#include "MessageController.h"


MessageController::MessageController(Json::Value config)
{
	this->rootPath = JsonUtil::toString(config["rootpath"]);
  	addRoutes(config["routes"]);
}

void MessageController::addRoutes(Json::Value routesconfig){
	setRoutes(
			new Route("GET", routesconfig["GET"].asString(),
					&MessageController::get));
	setRoutes(
			new Route("POST", routesconfig["CREATE"].asString(),
					&MessageController::create));
	setRoutes(
			new Route("POST", routesconfig["BROADCAST"].asString(),
					&MessageController::sendBroadcast));
	setRoutes(
			new Route("GET", routesconfig["LAST"].asString(),
					&MessageController::getLast));
}

void MessageController::create(Request * req) 
{

	MessageService* messageService = ServiceProvider::getMessageService();
	string userFrom = req->getParamFromBody("userFrom");
	string userTo = req->getParamFromBody("userTo");
	string content = req->getParamFromBody("content");
	string typeOf = req->getParamFromBody("typeOf");
	string date = req->getParamFromBody("date");

	cout << "CREATE MESSAGE"  << endl;

	string result = "";
	try 
	{
		result = messageService->create(userFrom,userTo,content,typeOf,date);
		//GenericResponseDto dto("message","\"true\"");
		req->setRawResponse(result);
	} 
	catch (Exception e) 
	{
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

void MessageController::sendBroadcast(Request * req) 
{

	MessageService* messageService = ServiceProvider::getMessageService();
	string userFrom = req->getParamFromBody("userFrom");
	string content = req->getParamFromBody("content");
	string typeOf = req->getParamFromBody("typeOf");
	string date = req->getParamFromBody("date");

	cout << "CREATE MESSAGE"  << endl;

	string result = "";
	try 
	{
		result = messageService->sendBroadcast(userFrom, content, typeOf, date);
		req->setRawResponse("{\"usersSend\":"+result+"}");
	} 
	catch (Exception e) 
	{
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}


void MessageController::get(Request * req) 
{
	MessageService* messageService = ServiceProvider::getMessageService();
	string userFrom = req->getParamFromUri("userFrom");
	string userTo = req->getParamFromUri("userTo");

	cout << "GET ALL MESSAGE"  << endl;

	string result = "";
	try {
		result = messageService->getMessages(userFrom,userTo);
		req->setRawResponse(result);

	} 
	catch (Exception e) 
	{
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}

}

void MessageController::getLast(Request * req) 
{
	MessageService* messageService = ServiceProvider::getMessageService();
	string userName = req->getParamFromUri("userName");

	cout << "GET LASTS MESSAGE"  << userName<< endl;

	string result = "";
	try {
		result = messageService->getLastMessages(userName);

		req->setRawResponse("{\"messages\":"+result+"}");
	} 
	catch (Exception e) 
	{
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}

}


MessageController::~MessageController()
{

}

