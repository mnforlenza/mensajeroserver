/*
 * API para log in 
 */

#ifndef _LOGIN_CONTROLLER_H
#define _LOGIN_CONTROLLER_H
#include "ServiceProvider.h"
#include "Controller.h"
#include "mongoose.h"


class LoginController : public Controller
{
	public:

		LoginController(Json::Value config);

		static void signin(Request * req);
		static void signout(Request * req);

		virtual ~LoginController();

	protected:
		void addRoutes(Json::Value routesconfig);
};

#endif /* _LOGIN_CONTROLLER_H */
