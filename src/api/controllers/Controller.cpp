/*
 *
 */


#include "Controller.h"


Controller::Controller()
{
}

void Controller::setRoutes(Route *route)
{
	this->routes.push_back(route);
}

void Controller::handle(Request *req)
{

	for(size_t i=0;i<this->routes.size();++i) 
	{

		if (this->routes[i]->httpMethod == req->getMethod() && this->routes[i]->url == req->getUri())
		{
			this->routes[i]->function(req);
		}
	}

}

void Controller::addRoutes(Json::Value routesconfig){

}

Controller::~Controller()
{

}

