/*
 *
 */

#ifndef _CONTROLLER_H
#define _CONTROLLER_H
#include "Route.h"
#include <json/json.h>
#include <vector>
#include <string>
#include "Request.h"


using namespace std;

class Controller
{
	public:
		Controller();
		virtual ~Controller();
		void handle(Request *req);
		void setRoutes(Route *route);
		string rootPath; 
	protected:
		vector<Route *> routes;
		virtual void addRoutes(Json::Value routesconfig);
};

#endif /* _CONTROLLER_H */
