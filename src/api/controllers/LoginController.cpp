#include "LoginController.h"

LoginController::LoginController(Json::Value config)
{
	this->rootPath = JsonUtil::toString(config["rootpath"]);
  	addRoutes(config["routes"]);
}

void LoginController::addRoutes(Json::Value routesconfig)
{

  	setRoutes(new Route("POST",routesconfig["POST"].asString(),&LoginController::signin));
  	setRoutes(new Route("GET",routesconfig["GET"].asString(),&LoginController::signout));

}

void LoginController::signin(Request * req)
{
  cout<<"Paso";
  string dato1 = req->getParamFromUri("dato1");
  string n1 =  req->getParamFromBody("n1");
  UserServiceImpl* userService = ServiceProvider::getUserService();
  bool result = userService->isUserOnline(n1);
  if(result){
	  req->setResponseJson("true");
  }else{
	  req->setResponseJson("false");
  }
}

void LoginController::signout(Request * req)
{
	cout << "signout"<<endl;

}

LoginController::~LoginController()
{
	for ( size_t i = 0; i < routes.size(); i++)
	{
	    delete routes[i];
	}

}

