#include "UserController.h"

UserController::UserController(Json::Value config) {
	this->rootPath = JsonUtil::toString(config["rootpath"]);
	addRoutes(config["routes"]);
}

void UserController::addRoutes(Json::Value routesconfig) {
	setRoutes(
			new Route("GET", routesconfig["GET"].asString(),
					&UserController::get));
	setRoutes(
			new Route("GET", routesconfig["ONLINE"].asString(),
					&UserController::online));
	setRoutes(
			new Route("GET", routesconfig["GETUSERS"].asString(),
					&UserController::getUsers));
	setRoutes(
			new Route("POST", routesconfig["CREATE"].asString(),
					&UserController::create));
	setRoutes(
			new Route("PUT", routesconfig["PUT"].asString(),
					&UserController::put));
	setRoutes(
			new Route("POST", routesconfig["LOGIN"].asString(),
					&UserController::login));
	setRoutes(
			new Route("POST", routesconfig["LOGOUT"].asString(),
					&UserController::logout));
	setRoutes(
				new Route("PUT", routesconfig["CHECKIN"].asString(),
						&UserController::checkin));
}

void UserController::create(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromBody("userName");
	string password = req->getParamFromBody("password");
	string email = req->getParamFromBody("email");

	cout << "CREATE SERVICE " << userName << " " << password << " " << email
			<< endl;

	string result = "";
	try {
		result = userService->create(userName, password, email).toJson();
		GenericResponseDto dto("user", result);
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}


void UserController::put(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromBody("userName");
	string password = req->getParamFromBody("password");
	string email = req->getParamFromBody("email");
	string photo = req->getParamFromBody("photo");

	cout << "PUT SERVICE " << userName << " "  << endl;
	string result = "";
	try {
		result = userService->update(userName, email, photo).toJson();
		GenericResponseDto dto("user", result);
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

void UserController::checkin(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromBody("userName");
	string latitude = req->getParamFromBody("latitude");
	string longitude = req->getParamFromBody("longitude");

	cout << "CHECKIN SERVICE " << userName << " " << endl;
	string result = "";
	try {
		result = userService->updateLocation(userName, latitude, longitude).toJson();
		GenericResponseDto dto("user", result);
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

void UserController::login(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromBody("userName");
	string password = req->getParamFromBody("password");

	cout << "LOGIN SERVICE " << userName << " " << password << " " << endl;

	string result = "";
	try {
		result = userService->login(userName, password);
		GenericResponseDto dto("token", "\"" + result + "\"");
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}

}

void UserController::logout(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromBody("userName");
	cout << "LOGOUT SERVICE " << userName << endl;
	string result = "";
	try {
		result = userService->logout(userName);
		GenericResponseDto dto("token", "\"" + result + "\"");
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

void UserController::get(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromUri("userName");
	cout << "GET SERVICE " << userName << endl;

	string result = "";
	try {
		result = userService->getUser(userName).toJson();
		GenericResponseDto dto("user", result);
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}


void UserController::getUsers(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	//string userName = req->getParamFromUri("userName");
	cout << "GET SERVICE USERS" << " " << endl;

	string result = "";
	try 
	{
		result = userService->getUsers();
		req->setRawResponse("{\"users\":"+result+"}");
		
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

void UserController::online(Request * req) {
	UserServiceImpl* userService = ServiceProvider::getUserService();
	string userName = req->getParamFromUri("userName");
	cout << "ONLINE SERVICE " << userName << endl;

	string result = "";
	try {
		bool online = userService->isUserOnline(userName);
		if(online){
			result = "\"true\"";
		}else{
			result = "\"false\"";
		}
		GenericResponseDto dto("online", result);
		req->setRawResponse(dto.toJson());
	} catch (Exception e) {
		GenericResponseDto dto(e);
		req->setRawResponse(dto.toJson());
	}
}

UserController::~UserController() {

}

