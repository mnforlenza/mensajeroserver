/*
 * API para mensajes
 */

#ifndef _MESSAGE_CONTROLLER_H
#define _MESSAGE_CONTROLLER_H
#include "Controller.h"
 #include "ServiceProvider.h"
#include "GenericResponseDto.h"


class MessageController : public Controller
{
	public:
		MessageController(Json::Value config);
	    static void create(Request * req);
	    static void sendBroadcast(Request * req);
	    static void get(Request * req);
	    static void getLast(Request * req);
		virtual ~MessageController();

	protected:
		void addRoutes(Json::Value routesconfig);

};

#endif /* _MESSAGE_CONTROLLER_H */