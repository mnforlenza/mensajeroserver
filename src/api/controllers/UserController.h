/*
 * API para usuario
 */

#ifndef _USER_CONTROLLER_H
#define _USER_CONTROLLER_H
#include "Controller.h"
#include "ServiceProvider.h"
#include "User.h"
#include "GenericResponseDto.h"

class UserController : public Controller
{
	public:
		UserController(Json::Value config);
		virtual ~UserController();

		static void create(Request * req);
		static void get(Request * req);
		static void getUsers(Request * req);
		static void online(Request * req);
		static void put(Request * req);
		static void login(Request * req);
		static void logout(Request * req);
		static void checkin(Request * req);

	protected:
		void addRoutes(Json::Value routesconfig);

};

#endif /* _USER_CONTROLLER_H */
