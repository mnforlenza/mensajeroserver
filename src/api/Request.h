#ifndef _REQUEST_H
#define _REQUEST_H

#include <string>
#include <iostream>
#include <sstream>
#include "mongoose.h"
#include "JsonUtil.h"


using namespace std;


class Request
{
	public:
		Request(struct mg_connection *conn);
        ~Request();
        string getParamFromUri(string param);
        string getParamFromBody(string param);
		void setResponseJson(string param);
		void setRawResponse(string json);
		string getUri();
		string getMethod();
		void printData();

	private:
		struct mg_connection *conn;
		string body;
		string params;
		string uri;
		string method;
		Json::Value jsonData;

};


#endif /* _REQUEST_H */
