#include "DaoMessage.h"

DaoMessage::DaoMessage(Configuration& conf) {
	this->dbManager = DbManager::getInstance(conf);
}

DaoMessage::~DaoMessage() 
{
}

/**
 * Busca un mensaje
 * @param key clave de búsqueda
 * @return el mensaje encontrado
 */
string DaoMessage::find(string key) 
{

	string value;
	if (dbManager->get(key, value).ok())
	{ 
		Json::Value root;
		Json::Reader reader;
		bool parsedSuccess = reader.parse(value, root, false);

		if (!parsedSuccess) 
		{
			throw Exception("Error al parsear mensajes");
		}

	}
	
	return value;
}

/**
 * Guarda un mensaje
 * @param key clave del mensaje
 * @param message mensaje a agregar
 */
void DaoMessage::save(string key, Message message) 
{
	string value;
	string actual = this->find(key);
	if (!actual.empty())
	{
		actual.erase(actual.end()-1);
		actual.erase(actual.begin());
		actual = actual + ",";
	}

	string json = "["  + actual  + message.toJson() + "]";
	dbManager->put(key, json);
}

/**
 * Guarda el último mensaje
 * @param key clave del mensaje
 * @param message mensaje a agregar
 */
void DaoMessage::saveLastMessage(string key, Message message) 
{
	string value;
	string actual = this->find(key);
	string messageString = message.toJson();
	bool hasMessage = false;
	if (!actual.empty())
	{


		Json::Value root;
		Json::Reader reader;
		bool parsedSuccess = reader.parse(actual, root, false);

		//Recorro el array con los id de usuarios
		for (unsigned int index = 0; index < root.size(); ++index) 
		{ 
			Json::Value value = root[index];
			if ((message.getUserTo().compare(root[index]["userTo"].asString())==0) && 
				(message.getUserFrom().compare(root[index]["userFrom"].asString())==0))
			{	

				root[index]["typeOf"] = message.getTypeOf();
				root[index]["content"] = message.getContent();
				root[index]["date"] = message.getDate();

				hasMessage = true;
			}
		}

		if (hasMessage)
		{
			Json::FastWriter writer;
			messageString = "";
			actual = writer.write(root);
			actual.erase(actual.end()-1);
		}

		actual.erase(actual.end()-1);
		actual.erase(actual.begin());

		if (!hasMessage)
		{
			actual = actual + ",";
		}
	}

	string json = "["  + actual  + messageString + "]";

	dbManager->put(key, json);
}

