/*
 * DaoUser.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef DAOMESSAGE_H_
#define DAOMESSAGE_H_

#ifndef MESSAGE_H_
#include "Message.h"
#endif

#ifndef CONFIGURATION_H_
#include "Configuration.h"
#endif

#include "Exception.h"


#ifndef DBMANAGER_H_
#include "DbManager.h"
#endif

#include "rocksdb/db.h"
#include "json/json.h"

#include <iostream>

using namespace std;

/**
 * Clase que se encarga del acceso a datos de los mensajes
 */
class DaoMessage {
public:
	DaoMessage(Configuration& conf);
	void save(string key, Message message);
	void saveLastMessage(string key, Message message);
	string find(string message);
	~DaoMessage();
private:
	DbManager* dbManager;
};

#endif /* DAOUSER_H_ */
