/*
 * DbManager.cpp
 *
 *  Created on: 28/3/2015
 *      Author: marcos
 */

#include "DbManager.h"

bool DbManager::instanceFlag = false;
DbManager* DbManager::single = NULL;

DbManager::DbManager(Configuration& config) {
    options.create_if_missing = true;
    Status status = rocksdb::DB::Open(options, config.getDbDirectory().c_str(), &db);
}

DbManager::~DbManager() {
	delete single;
	delete db;
}

/**
 * Devuelve el Singleto de manejador de base de datos
 * @param config configuración
 * @return la instancia única del DbManager
 */
DbManager* DbManager::getInstance(Configuration& config){
	if(!instanceFlag){
		single = new DbManager(config);
        instanceFlag = true;
	}
	return single;
}

/**
 * Wrapper al get de rocksdb
 */
Status DbManager::get(string key, string& value){
	Status result = db->Get(rocksdb::ReadOptions(), key, &value);
	return result;
}

/**
 * Wrapper al set de rocksdb
 */
Status DbManager::put(string key, string value){
	return db->Put(rocksdb::WriteOptions(), key, value);
}

