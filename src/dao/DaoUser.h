/*
 * DaoUser.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef DAOUSER_H_
#define DAOUSER_H_

#ifndef USER_H_
#include "User.h"
#endif

#ifndef CONFIGURATION_H_
#include "Configuration.h"
#endif

#ifndef SRC_EXCEPTION_USERNOTFOUNDEXCEPTION_H_
#include "UserNotFoundException.h"
#endif

#ifndef SRC_EXCEPTION_USERNAMEUNAVAILABLEEXCEPTION_H_
#include "UserNameUnavailableException.h"
#endif

#ifndef DBMANAGER_H_
#include "DbManager.h"
#endif

#include "rocksdb/db.h"
#include "json/json.h"
#include <vector>
#include <algorithm>
#include <iostream>

using namespace std;

/**
 * Clase que se encarga del acceso a datos de los usuarios
 */
class DaoUser {
public:
	DaoUser(Configuration& conf);
	User persist(User user);
	User update(User user);
	User find(string userName);
	vector<string> findUsers(string key);
	string findList(string key);
	void saveToList(string key, string userName);
	bool isUserOnline(string userName);
	string getUserPassword(string userName);
	string setStatus(string userName, bool online);
	void setActive(string userName, bool active);
	~DaoUser();
private:
	DbManager* dbManager;
};

#endif /* DAOUSER_H_ */
