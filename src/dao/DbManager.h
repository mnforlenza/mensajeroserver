/*
 * DbManager.h
 *
 *  Created on: 28/3/2015
 *      Author: marcos
 */

#ifndef DBMANAGER_H_
#define DBMANAGER_H_

#include "Configuration.h"
#include "rocksdb/db.h"
#include <iostream>

using namespace std;

using namespace rocksdb;

/**
 * Clase para la conexiones a la base de datos
 */
class DbManager {
public:
	~DbManager();
	static DbManager* getInstance(Configuration& config);
	Status get(string key, string& value);
	Status put(string key, string value);
private:
	DB* db;
	Options options;
	static DbManager* single;
	static bool instanceFlag;
	DbManager(Configuration& config);
};
#endif /* DBMANAGER_H_ */
