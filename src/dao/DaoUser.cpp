/*
 * DaoUser.cpp
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#include "DaoUser.h"

DaoUser::DaoUser(Configuration& conf) {
	this->dbManager = DbManager::getInstance(conf);
}

DaoUser::~DaoUser() {
	//delete dbManager;
}

/**
 * Busca un usuario con el nombre de usuario pasado por parámetro
 * @param userName el nombre de usuario
 * @return el usuario encontrado
 */
User DaoUser::find(string userName) {
	string value;

	dbManager->get(userName, value);

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(value, root, false);

	if (!parsedSuccess) {
		throw UserNotFoundException();
	}
	return JsonUtil::userFromJson(value);
}

/**
 * Devuelve todos los usuarios en json
 * @param key clave de búsqueda
 * @return los usuarios encontrados
 */
vector<string> DaoUser::findUsers(string key)
{
	string value;
	vector<string> stringVec;
	if (dbManager->get(key, value).ok())
	{ 
		Json::Value root;
		Json::Reader reader;
		bool parsedSuccess = reader.parse(value, root, false);

		if (!parsedSuccess) 
		{
			throw UserNotFoundException();
		}

		//Obtengo los datos de los usuarios
		const Json::Value array = root;

		//Recorro el array con los id de usuarios
		for (unsigned int index = 0; index < array.size(); ++index) 
		{ 
			const Json::Value value = array[index];
    		stringVec.push_back(value["userName"].asString());
		}
	}

	return stringVec;
}

/**
 * Devuelve todos los usuarios en json
 * @param key clave de búsqueda
 * @return los usuarios encontrados
 */
string DaoUser::findList(string key) 
{

	string userList = "[";
	vector<string> stringVec;

	stringVec = this->findUsers(key);
	//Busco los datos de los usuarios
	if (stringVec.size()>0)
	{
		//Ordeno el array de usuarios
		sort(stringVec.begin(), stringVec.end());

 		for(std::size_t i=0;i<stringVec.size();++i)			
 		{
 			string user;
			string userKey = stringVec[i];

	    	if (dbManager->get(userKey, user).ok())
	    	{
	    		Json::Value rootUser;
				Json::Reader readerUer;
				bool parsedSuccessUser = readerUer.parse(user, rootUser, false);

				if (!parsedSuccessUser) 
				{
					throw UserNotFoundException();
				}

				userList = userList + user + ",";

	    	}
    	}

		if (stringVec.size()>0)
			userList.erase(userList.end()-1);
    }
	userList = userList + "]";

	return userList;
}



/**
 * Guarda un usuario a la lista de nombre de usuarios
 * @param key clave a guardar
 * @param userName el nombre de usuario
 */
void DaoUser::saveToList(string key, string userName) 
{
	string value;
	if (dbManager->get(key, value).ok())
	{ 
		Json::Value root;
		Json::Reader reader;
		bool parsedSuccess = reader.parse(value, root, false);

		if (!parsedSuccess) 
		{
			throw UserNotFoundException();
		}
	}
	
	string actual = value;

	//Se checkea que el array no este vacio
	if (!actual.empty())
	{
		actual.erase(actual.end()-1);
		actual.erase(actual.begin());
		actual = actual + ",";
	}

	string json = "["  + actual  + "{\"" + "userName" + "\"" + ":\"" + userName  + "\""+"}" + "]";
	dbManager->put(key, json);
}

/**
 * Consulta en la base si el usuario pasado por parámetro esta conectado o no
 * @param userName el nombre de usuario
 * @return true en caso afirmativo, false en caso contrario
 */
bool DaoUser::isUserOnline(string userName) {
	string value;

	dbManager->get(userName, value);

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(value, root, false);

	if (!parsedSuccess) {
		throw UserNotFoundException();
	}

	const Json::Value onlineJson = root["online"];

	if (onlineJson.asString() == "true") {
		return true;
	} else {
		return false;
	}

}

/**
 * Devuelve un password asociado a un usuario
 * @param userName el nombre de usuario
 * @return el password de ese usuario
 */
string DaoUser::getUserPassword(string userName) {
	string value;

	dbManager->get(userName, value);

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(value, root, false);

	if (!parsedSuccess) {
		throw UserNotFoundException();
	}

	const Json::Value onlineJson = root["password"];
	return onlineJson.asString();
}

/**
 * Persiste un usuario
 * @param user el usuario
 */
User DaoUser::persist(User user) {
	string value;

	dbManager->get(user.getUserName(), value);

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(value, root, false);

	if (parsedSuccess) {
		User aux = JsonUtil::userFromJson(value);
		if(aux.isActive()){
			throw UserNameUnavailableException();
		}
	}

	string json = user.toJson();
	dbManager->put(user.getUserName(), json);

	//Se graba en un array al usuario
	this->saveToList("_users",user.getUserName());

	return user;
}

/**
 * Actualiza un usuario
 * @param user el usuario
 */
User DaoUser::update(User user) {
	string json = user.toJson();
	dbManager->put(user.getUserName(), json);
	return user;
}

/**
 * Modifica el status del usuario
 * @param userName nombre de usuario
 * @param online si se desea setear online o no
 */
string DaoUser::setStatus(string userName, bool online) {
	string value;
	dbManager->get(userName, value);
	User user = JsonUtil::userFromJson(value);
	if (online) {
		user.setOnline();
	} else {
		user.setOffline();
	}
	dbManager->put(userName, user.toJson());
	return userName;
}

/**
 * Modifica el status del usuario
 * @param userName nombre de usuario
 * @param online si se desea setear online o no
 */
void DaoUser::setActive(string userName, bool active) {
	string value;
	dbManager->get(userName, value);
	User user = JsonUtil::userFromJson(value);
	if (active) {
		user.activeUser();
	} else {
		user.deactiveUser();
	}
	dbManager->put(userName, user.toJson());
}
