#ifndef MESSAGE_H_
#define MESSAGE_H_

#include "JsonUtil.h"
#include <iostream>

using namespace std;

/**
 * Clase que representa a una entidad mensaje
 */
class Message {
public:
	Message(string userFrom, string userTo, string content, string typeOf, string date);
	string getUserFrom();
	string getUserTo();
	string getTypeOf();
	string getContent();
	string getDate();	
	string toJson();
	~Message();

private:
	string userFrom;
	string userTo;
	string content;
	string date;
	string typeOf;


};

#endif /* USER_H_ */
