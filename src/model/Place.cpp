/*
 * Place.cpp
 *
 *  Created on: 14/6/2015
 *      Author: marcos
 */

#include "Place.h"

Place::Place(string latitude, string longitude, string description) {
	this->latitude = latitude;
	this->longitude = longitude;
	this->description = description;
}

Place::~Place() {

}

string Place::getDescription(){
	return this->description;
}

string Place::getLocation(){
	return this->latitude + ":" + this->longitude;
}

/**
 * Calcula la distancia entre la instancia corriente y otra instancia pasada por parámetro
 * @param anotherPlace otro lugar
 * @return la distancia entre los dos lugares
 */
double Place::calculateDistance(Place anotherPlace){
	double result = 0;
	double x = stod(this->latitude, NULL);
	double y = stod(this->longitude, NULL);
	double xAnother = stod(anotherPlace.latitude, NULL);
	double yAnother = stod(anotherPlace.longitude, NULL);
	double aux1 = x - xAnother;
	double aux2 = y - yAnother;
	result = sqrt(pow(aux1,2.0) + pow(aux2,2.0));
	return result;
}
