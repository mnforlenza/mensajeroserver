#include "Message.h"

Message::Message(string userFrom, string userTo, string content, string typeOf, string date) {
	this->userFrom = userFrom;
	this->userTo = userTo;
	this->content = content;
	this->typeOf = typeOf;
	this->date = date;
}


Message::~Message() {
}

string Message::getUserFrom() 
{
	return this->userFrom;
}

string Message::getUserTo() 
{
	return this->userTo;
}

string Message::getTypeOf() 
{
	return  this->typeOf;
}

string Message::getContent()
{
	return  this->content;
}

string Message::getDate() 
{
	return this->date;
}

/**
 * Transforma un mensaje a json
 * @return el json que representa a ese mensaje
 */
string Message::toJson(){
	string result = "{\"";
	result = result + "userFrom" + "\"" + ":\"" + this->userFrom + "\",";
	result = result + "\"" + "userTo" + "\"" + ":\"" + this->userTo + "\",";
	result = result + "\"" + "content" + "\"" + ":\"" + this->content + "\"";
	result = result + ",\"" + "date" + "\"" + ":\"" + this->date + "\"";
	result = result + ",\"" + "typeOf" + "\"" + ":\"" + this->typeOf + "\"";
	result += "}";
	return result;
}
