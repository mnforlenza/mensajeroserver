/*
 * User.cpp
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#include "User.h"

User::User(string userName, string password) {
	this->userName = userName;
	this->password = password;
	this->online = false;
	this->active = false;
	this->location = "";
	this->locationPlace = "";
	this->locationPlaceDescription = "";
}

User::~User() {
	// TODO Auto-generated destructor stub
}

void User::changePassword(string newPassword){
	this->password = newPassword;
}

void User::activeUser(){
	this->active = true;
}

void User::deactiveUser(){
	this->active = false;
}

string User::getEmail(){
	return this->email;
}

string User::getPassword(){
	return this->password;
}

string User::getUserName(){
	return this->userName;
}

void User::setEmail(string email){
	this->email = email;
}

void User::update(string email,string photo){
	this->email = email;
	this->photo = photo;
}

void User::updateLocation(string location, string locationPlace, string locationPlaceDescription){
	this->location = location;
	this->locationPlace = locationPlace;
	this->locationPlaceDescription = locationPlaceDescription;
}

string User::getLocation(){
	return this->location;
}

string User::getLocationPlace(){
	return this->locationPlace;
}

string User::getLocationPlaceDescription(){
	return this->locationPlaceDescription;
}

void User::setOffline(){
	this->online = false;
}

void User::setOnline(){
	this->online = true;
}

bool User::isActive(){
	return this->active;
}

/**
 * Transforma el usuario a json
 * @return el json que representa a ese usuario
 */
string User::toJson(){
	string result = "{\"";
	result = result + "userName" + "\"" + ":\"" + this->userName + "\",";
	result = result + "\"" + "password" + "\"" + ":\"" + this->password + "\",";
	result = result + "\"" + "email" + "\"" + ":\"" + this->email + "\",";
	result = result + "\"" + "photo" + "\"" + ":\"" + this->photo + "\",";
	result = result + "\"" + "location" + "\"" + ":\"" + this->location + "\",";
	result = result + "\"" + "locationPlace" + "\"" + ":\"" + this->locationPlace + "\",";
	result = result + "\"" + "locationPlaceDescription" + "\"" + ":\"" + this->locationPlaceDescription + "\",";
	result = result + JsonUtil::toJson("active",this->active) + ",";
	result = result + JsonUtil::toJson("online",this->online);
	result += "}";
	return result;
}
