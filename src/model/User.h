/*
 * User.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef USER_H_
#define USER_H_

#ifndef JSONUTIL_H_
#include "../util/JsonUtil.h"
#endif

#include <iostream>

using namespace std;

/**
 * Clase que representa a un usuario
 */
class User {
public:
	User(string userName, string password);
	void changePassword(string newPassword);
	string getUserName();
	string getPassword();
	string getEmail();
	void setEmail(string email);
	void update(string email,string photo);
	void updateLocation(string location, string locationPlace, string locationPlaceDescription);
	string getLocation();
	string getLocationPlace();
	string getLocationPlaceDescription();
	void activeUser();
	void deactiveUser();
	void setOnline();
	void setOffline();
	bool isActive();
	string toJson();
	~User();

private:
	string userName;
	string password;
	string email;
	string photo;
	string location;
	string locationPlace;
	string locationPlaceDescription;
	bool active;
	bool online;
};

#endif /* USER_H_ */
