/*
 * Place.h
 *
 *  Created on: 14/6/2015
 *      Author: marcos
 */

#ifndef SRC_MODEL_PLACE_H_
#define SRC_MODEL_PLACE_H_

#include <math.h>
#include <iostream>

using namespace std;

/**
 * Clase que representa a una entidad lugar
 */
class Place {

public:
	Place(string latitude, string longitude, string description);
	double calculateDistance(Place anotherPlace);
	string getDescription();
	string getLocation();
	~Place();
private:
	string latitude;
	string longitude;
	string description;

};

#endif /* SRC_MODEL_PLACE_H_ */
