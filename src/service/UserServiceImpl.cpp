/*
 * UserServiceImpl.cpp
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#include "UserServiceImpl.h"

UserServiceImpl::UserServiceImpl(DaoUser* daoUser) {
	this->daoUser = daoUser;

}

UserServiceImpl::~UserServiceImpl() {
	// TODO Auto-generated destructor stub
}

UserService::~UserService() {
	// TODO Auto-generated destructor stub
}

/**
 * Servicio para obtener un usuario pasado por parámetro
 * @param userName el nombre de usuario a buscar
 * @return usuario con el nombre de usuario buscado por parametro
 */
User UserServiceImpl::getUser(string userName){
	return this->daoUser->find(userName);
}

/**
 * Servicio para obtener todos los nombres de usuario
 * @return todos los nombres de usuarios como un array json
 */
string UserServiceImpl::getUsers(){
	
	string users = this->daoUser->findList("_users");
	
	if (users.empty())
	{
		users = "[]";
	}
	return users;
}

/**
 * Servicio para comprobar si cierto usuario está online o no
 * @param userName el nombre de usuario a buscar
 * @return true si está online, false en caso contrario
 */
bool UserServiceImpl::isUserOnline(string userName) {
	return this->daoUser->isUserOnline(userName);
}

/**
 * Servicio para loguear un usuario
 * @param userName el nombre de usuario a loguear
 * @param password el password del usuario
 * @return el nombre de usuario logueado, excepción en caso de password incorrecto o nombre de usuario inválido
 */
string UserServiceImpl::login(string userName, string password) {
	string passwordResult = this->daoUser->getUserPassword(userName);
	if(passwordResult.compare(password)!=0){
		throw WrongPasswordException();
	}
	this->daoUser->setStatus(userName, true);
	return userName;
}

/**
 * Servicio para desloguear un usuario
 * @param userName el nombre de usuario a loguear
 */
string UserServiceImpl::logout(string userName) {
	return this->daoUser->setStatus(userName, false);
}

/**
 * Servicio para marcar un usuario como en línea
 * @param userName el nombre de usuario
 */
string UserServiceImpl::setOnline(string userName){
	return this->daoUser->setStatus(userName, true);
}

/**
 * Servicio para crear un usuario
 * @param userName el nombre de usuario a crear
 * @param password el password del usuario
 * @param email el email del usuario
 * @return el usuario creado
 */
User UserServiceImpl::create(string userName, string password, string email) {
	User user(userName, password);
	user.setEmail(email);
	user.setOffline();
	user.activeUser();
	return this->daoUser->persist(user);
}

/**
 * Servicio para actualizar la información de un usuario
 * @param userName el nombre de usuario a actualizar
 * @param email el email del usuario
 * @param photo la foto del usuario
 * @return el usuario actualizado
 */
User UserServiceImpl::update(string userName, string email, string photo) {
 	User user = this->daoUser->find(userName);
 	user.update(email,photo);
	return this->daoUser->update(user);
}

/**
 * Servicio para actualizar la información delocalización de un usuario
 * @param userName el nombre de usuario a actualizar
 * @param latitude latitud
 * @param longitude longitud
 * @return el usuario actualizado
 */
User UserServiceImpl::updateLocation(string userName, string latitude, string longitude){
 	User user = this->daoUser->find(userName);
 	InstancePlaces* instance = InstancePlaces::getInstance();
 	Place nearest = instance->getNearestPlace(latitude, longitude);
 	user.updateLocation(latitude+":"+longitude, nearest.getLocation(), nearest.getDescription());
 	return this->daoUser->update(user);
}

/**
 * Servicio para eliminar un usuario
 * @param userName el nombre de usuario a eliminar
 */
void UserServiceImpl::removeUser(string userName){
	this->daoUser->setActive(userName, false);
}

/**
 * Servicio para activar un usuario
 * @param userName el nombre de usuario a activar
 */
void UserServiceImpl::activeUser(string userName){
	this->daoUser->setActive(userName, true);
}
