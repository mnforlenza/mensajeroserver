/*
 * UserService.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef USERSERVICE_H_
#define USERSERVICE_H_

#ifndef USER_H_
#include "../model/User.h"
#endif

#include <iostream>

using namespace std;

/**
 * Interfáz para servicios de usuarios
 */
class UserService {
public:
	virtual User create(string userName, string password, string email) = 0;
	virtual User getUser(string userName)=0;
	virtual bool isUserOnline(string userName)=0;
	virtual string login(string userName, string password)=0;
	virtual string logout(string userName)=0;
	virtual string setOnline(string userName)=0;
	virtual void removeUser(string userName)=0;
	virtual void activeUser(string userName)=0;
	virtual User updateLocation(string userName, string latitude, string longitude)=0;
	virtual ~UserService()=0;
};

#endif /* USERSERVICE_H_ */
