/*
 * ServiceProvider.h
 *
 *  Created on: 12/4/2015
 *      Author: marcos
 */

#ifndef SERVICEPROVIDER_H_
#define SERVICEPROVIDER_H_

#include "UserService.h"
#include "UserServiceImpl.h"
#include "DaoUser.h"
#include "MessageService.h"
#include "DaoMessage.h"
#include "ConfigurationBuilder.h"
#include "Configuration.h"

#include <iostream>

using namespace std;

/**
 * Clase utilitaria para obtener servicios
 */
class ServiceProvider {
public:
	~ServiceProvider();
	static UserServiceImpl* getUserService();
	static MessageService* getMessageService();
private:
	ServiceProvider();
	static UserServiceImpl* singleUserService;
	static MessageService* singleMessageService;
	static void initializeServices();
	static bool instanceFlag;
};

#endif /* SERVICEPROVIDER_H_ */
