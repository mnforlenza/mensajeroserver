#ifndef MESSAGESERVICE_H_
#define MESSAGESERVICE_H_

#ifndef DAOMESSAGE_H
#include "DaoMessage.h"
#endif

#ifndef DAOUSER_H_
#include "DaoUser.h"
#endif

#include <iostream>

using namespace std;

/**
 * Clase para servicios de mensajería
 */
class MessageService
{
public:
	MessageService(DaoMessage* daoMessage, DaoUser* daoUser);
	virtual ~MessageService();
	virtual string create(string userFrom, string userTo, string content, string typeOf, string date);
	virtual string sendBroadcast(string userFrom, string content, string typeOf, string date);
	virtual string getMessages(string userFrom, string userTo);
	virtual string getLastMessages(string user);


private:
	DaoMessage* daoMessage;
	DaoUser* daoUser;
};

#endif /* MESSAGESERVICE_H_ */
