#include "MessageService.h"

MessageService::MessageService(DaoMessage* daoMessage, DaoUser* daoUser) 
{
	this->daoMessage = daoMessage;
	this->daoUser = daoUser;
}

MessageService::~MessageService() 
{
	// TODO Auto-generated destructor stub
}

/**
 * Servicio para crear un mensaje
 * @param userFrom usuario que envía
 * @param userTo usuario que recibe
 * @param content contenido del mensaje
 * @param typeOf tipo de mensaje
 * @param date fecha del mensaje
 * @return el mensaje creado en json
 */ 
string MessageService::create(string userFrom, string userTo, string content, string typeOf, string date) 
{

	Message message(userFrom, userTo, content, typeOf, date);

	string key;
	if(userFrom > userTo)
		key = userFrom + userTo;
	else
		key = userTo + userFrom;

	this->daoMessage->save(key, message);

	if(typeOf.compare("text")!=0)
	{
		content = typeOf;
	}

	Message lastMessageFrom(userFrom, userTo, content, typeOf, date);

	Message lastMessageTo(userTo, userFrom, content, typeOf, date);

	this->daoMessage->saveLastMessage("last_msg_"+userFrom,lastMessageFrom);

	this->daoMessage->saveLastMessage("last_msg_"+userTo,lastMessageTo);

	return message.toJson();
}

/**
 * Servicio para enviar un mensaje general
 * @param userFrom usuario que envía
 * @param content contenido del mensaje
 * @param typeOf tipo de mensaje
 * @param date fecha del mensaje
 * @return el mensaje creado en json
 */ 
string MessageService::sendBroadcast(string userFrom, string content, string typeOf, string date) 
{
	vector<string> stringVec;
	int counter = 0;
	stringVec = this->daoUser->findUsers("_users");
	if (stringVec.size()>0)
	{
		for(std::size_t i=0;i<stringVec.size();++i)			
 		{
 			string userTo = stringVec[i];
 			if(userTo.compare(userFrom)!=0)
 			{
 				this->create(userFrom,userTo,content,typeOf,date);
 				counter++;
 			}

 		}
	}
	return to_string(counter);
}

/**
 * Servicio para obtener un mensaje
 * @param userFrom usuario que envía
 * @param userTo usuario que recibe
 * @return el mensaje en json
 */ 
string MessageService::getMessages(string userFrom, string userTo)
{	
	string key;
	if(userFrom > userTo)
		key = userFrom + userTo;
	else
		key = userTo + userFrom;

	string messages = this->daoMessage->find(key);
	if (messages.empty())
	{
		messages = "[]";
	}
	return messages;
}

/**
 * Servicio para obtener el último mensaje
 * @param user el usuario
 * @return el mensaje en json
 */ 
string MessageService::getLastMessages(string user)
{
	string messages = this->daoMessage->find("last_msg_"+user);
	if (messages.empty())
	{
		messages = "[]";
	}
	return messages;
}
