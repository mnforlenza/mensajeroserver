/*
 * UserServiceImpl.h
 *
 *  Created on: 14/3/2015
 *      Author: marcos
 */

#ifndef USERSERVICE_H_
#include "UserService.h"
#endif

#ifndef DAOUSER_H_
#include "DaoUser.h"
#endif

#ifndef USER_H_
#include "User.h"
#endif

#ifndef SRC_EXCEPTION_WRONGPASSWORDEXCEPTION_H_
#include "WrongPasswordException.h"
#endif

#ifndef USERSERVICEIMPL_H_
#define USERSERVICEIMPL_H_

#include "InstancePlaces.h"
#include <iostream>

using namespace std;

/**
 * Clase para servicios de usuarios
 */
class UserServiceImpl : public UserService{
public:
	UserServiceImpl(DaoUser* daoUser);
	virtual ~UserServiceImpl();
	virtual User create(string userName, string password, string email);
	virtual User update(string userName, string email, string photo);
	virtual User updateLocation(string userName, string latitude, string longitude);
	virtual User getUser(string userName);
	virtual string getUsers();
	virtual bool isUserOnline(string userName);
	virtual string login(string userName, string password);
	virtual string logout(string userName);
	virtual string setOnline(string userName);
	virtual void removeUser(string userName);
	virtual void activeUser(string userName);
private:
	DaoUser* daoUser;
};

#endif /* USERSERVICEIMPL_H_ */
