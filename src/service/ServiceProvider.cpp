/*
 * ServiceProvider.cpp
 *
 *  Created on: 12/4/2015
 *      Author: marcos
 */

#include "ServiceProvider.h"

bool ServiceProvider::instanceFlag = false;
UserServiceImpl* ServiceProvider::singleUserService = NULL;
MessageService* ServiceProvider::singleMessageService = NULL;


ServiceProvider::ServiceProvider() {
}

ServiceProvider::~ServiceProvider() {
	// TODO Auto-generated destructor stub
}

/**
 * Inicializa los servicios
 */ 
void ServiceProvider::initializeServices(){
	ConfigurationBuilder builder("./conf/conf.json");
	Configuration result = builder.build();
	DaoUser* daoUser = new DaoUser(result);
	DaoMessage* daoMessage = new DaoMessage(result);
	UserServiceImpl* userService = new UserServiceImpl(daoUser);
	MessageService* messageService = new MessageService(daoMessage,daoUser);
	singleUserService = userService;
	singleMessageService = messageService;
	instanceFlag = true;
}

/**
 * Devuelve una implementación del servicio de usuarios
 * @return una implementación del servicio de usuarios
 */ 
UserServiceImpl* ServiceProvider::getUserService(){
	if(!instanceFlag){
		initializeServices();
	}
	return singleUserService;
}

/**
 * Devuelve una implementación del servicio de mensajes
 * @return una implementación del servicio de mensajes
 */
MessageService* ServiceProvider::getMessageService(){
	if(!instanceFlag){
		initializeServices();
	}
	return singleMessageService;
}

