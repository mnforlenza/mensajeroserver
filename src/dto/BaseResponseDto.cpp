/*
 * BaseResponseDto.cpp
 *
 *  Created on: 19/4/2015
 *      Author: marcos
 */

#include "BaseResponseDto.h"

BaseResponseDto::BaseResponseDto() {
	this->succes = true;
	this->error = "";
}

BaseResponseDto::BaseResponseDto(Exception e) {
	this->succes = false;
	this->error = e.getMessage();
}

BaseResponseDto::~BaseResponseDto() {
	// TODO Auto-generated destructor stub
}

string BaseResponseDto::getError(){
	return this->error;
}

void BaseResponseDto::setError(string error){
	this->error = error;
}

/**
 * Transforma a json
 * @return un string con la representación en json
 */ 
string BaseResponseDto::toJson(){
	string result = "{";
	result += JsonUtil::toJson("succes", succes);
	result += ",";
	result += "\"error\":\"" + error + "\",";
	string bodyResult = toJsonResult();
	result += bodyResult;
	result += "}";
	return result;
}

