/*
 * GenericResponseDto.cpp
 *
 *  Created on: 26/4/2015
 *      Author: marcos
 */

#include "GenericResponseDto.h"

GenericResponseDto::GenericResponseDto(string name, string json) {
	this->name = name;
	this->json = json;
}

GenericResponseDto::GenericResponseDto(Exception e) : BaseResponseDto::BaseResponseDto(e) {
	this->name = "genericErrorResult";
	this->json = "{}";
}

GenericResponseDto::~GenericResponseDto() {
	// TODO Auto-generated destructor stub
}

string GenericResponseDto::toJson(){
	return BaseResponseDto::toJson();
}

/**
 * Devuelve un resultado genérico en json
 * @return el json del resultado genérico 
 */ 
string GenericResponseDto::toJsonResult(){
	string result = "\"result\":{";
	result+= "\"" + name + "\":" + json;
	result += "}";
	return result;
}
