/*
 * GenericResponseDto.h
 *
 *  Created on: 26/4/2015
 *      Author: marcos
 */

#ifndef SRC_DTO_GENERICRESPONSEDTO_H_
#define SRC_DTO_GENERICRESPONSEDTO_H_

#include "BaseResponseDto.h"
#include "Exception.h"
#include <iostream>

using namespace std;

/**
 * Clase genérica para todos los dtos que devuelven los controllers en formato json
 */
class GenericResponseDto : BaseResponseDto{
public:
	GenericResponseDto(Exception e);
	GenericResponseDto(string name, string json);
	virtual ~GenericResponseDto();
	virtual string toJsonResult();
	virtual string toJson();
private:
	string name;
	string json;
};

#endif /* SRC_DTO_GENERICRESPONSEDTO_H_ */
