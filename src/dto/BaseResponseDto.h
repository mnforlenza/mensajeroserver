/*
 * BaseResponseDto.h
 *
 *  Created on: 19/4/2015
 *      Author: marcos
 */

#ifndef SRC_DTO_BASERESPONSEDTO_H_
#define SRC_DTO_BASERESPONSEDTO_H_

#include "Exception.h"
#include "JsonUtil.h"
#include <iostream>

using namespace std;

/**
 * Clase básica para todos los dtos que devuelven los controllers en formato json
 */  
class BaseResponseDto {
public:
	BaseResponseDto();
	BaseResponseDto(Exception e);
	virtual ~BaseResponseDto();
	string getError();
	void setError(string error);
	virtual string toJson();
	virtual string toJsonResult()=0;
private:
	bool succes;
	string error;
};

#endif /* SRC_DTO_BASERESPONSEDTO_H_ */
