/*
 * ConfigurationBuilder.cpp
 *
 *  Created on: 27/3/2015
 *      Author: marcos
 */

#include "ConfigurationBuilder.h"

ConfigurationBuilder::ConfigurationBuilder(string configurationFilePath) {
	string line;
	string result = "";
	ifstream myfile(configurationFilePath);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			result += line;
		}
		myfile.close();
	}
	this->json = result;
}

ConfigurationBuilder::~ConfigurationBuilder() {
	// TODO Auto-generated destructor stub
}

/**
 * Construye un objeto Configuration leyendo desde el json de configuración
 * @return el objeto Configuration
 */
Configuration ConfigurationBuilder::build() {
	Configuration result;

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(this->json, root, false);

	const Json::Value testEnabled = root[TEST_ENABLED];
	const Json::Value serverPort = root[SERVER_PORT];
	const Json::Value loggerLevel = root[LOGGER_LEVEL];
	const Json::Value loggerName = root[LOGGER_NAME];
	const Json::Value testDbDirectory = root[TEST_DB_DIRECTORY];
	const Json::Value prodDbDirectory = root[PROD_DB_DIRECTORY];

	if (testEnabled.asString() == "true") {
		result.setTestEnabled(true);
	} else {
		result.setTestEnabled(false);
	}
	result.setLoggerLevel(loggerLevel.asString());
	result.setLoggerName(loggerName.asString());
	result.setTestDbDirectory(testDbDirectory.asString());
	result.setProdDbDirectory(prodDbDirectory.asString());
	result.setServerPort(serverPort.asString());
	return result;
}
