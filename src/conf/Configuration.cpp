/*
 * Configuration.cpp
 *
 *  Created on: 26/3/2015
 *      Author: marcos
 */

#include "Configuration.h"

Configuration::Configuration() {
	this->testEnabled = false;
	this->serverPort = "";
	this->loggerName = "";
}

Configuration::~Configuration() {
	// TODO Auto-generated destructor stub
}

string Configuration::getServerPort(){
	return this->serverPort;
}

void Configuration::setServerPort(string serverPort){
	this->serverPort = serverPort;
}

string Configuration::getLoggerLevel(){
	return this->loggerLevel;
}

void Configuration::setLoggerLevel(string loggerLevel){
	this->loggerLevel = loggerLevel;
}

string Configuration::getLoggerName(){
	return this->loggerName;
}

void Configuration::setLoggerName(string loggerName){
	this->loggerName = loggerName;
}

bool Configuration::isTestEnabled(){
	return this->testEnabled;
}

void Configuration::setTestEnabled(bool testEnabled){
	this->testEnabled = testEnabled;
}

string Configuration::getTestDbDirectory(){
	return this->testDbDirectory;
}

void Configuration::setTestDbDirectory(string testDbFileSystem){
	this->testDbDirectory = testDbFileSystem;
}

string Configuration::getProdDbDirectory(){
	return this->prodDbDirectory;
}

void Configuration::setProdDbDirectory(string prodDbDirectory){
	this->prodDbDirectory = prodDbDirectory;
}

string Configuration::getDbDirectory(){
	if (this->isTestEnabled()){
		return this->testDbDirectory;
	}
	return this->prodDbDirectory;
}
