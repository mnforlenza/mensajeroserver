/*
 * Configuration.h
 *
 *  Created on: 26/3/2015
 *      Author: marcos
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <iostream>

using namespace std;

/**
 * Entidad que representa una configuración del server
 */
class Configuration {
public:
	Configuration();
	~Configuration();
	string getServerPort();
	void setServerPort(string serverPort);
	string getLoggerLevel();
	void setLoggerLevel(string loggerLevel);
	string getLoggerName();
	void setLoggerName(string loggerName);
	bool isTestEnabled();
	void setTestEnabled(bool testEnabled);
	string getTestDbDirectory();
	void setTestDbDirectory(string testDbDirectory);
	string getProdDbDirectory();
	void setProdDbDirectory(string prodDbDirectory);
	string getDbDirectory();
private:
	string  serverPort;
	string loggerLevel;
	string loggerName;
	bool testEnabled;
	string testDbDirectory;
	string prodDbDirectory;
};


#endif /* CONFIGURATION_H_ */
