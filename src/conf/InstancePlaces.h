/*
 * InstancePlaces.h
 *
 *  Created on: 14/6/2015
 *      Author: marcos
 */

#ifndef SRC_CONF_INSTANCEPLACES_H_
#define SRC_CONF_INSTANCEPLACES_H_

#include "Place.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <json/json.h>

#define PLACES_CONF_PATH "./conf/places.json"

using namespace std;

/**
 * Clase que se encarga de modelizar los distintos lugares configurados para esa instancia
 */
class InstancePlaces {
public:
	static InstancePlaces* getInstance();
	~InstancePlaces();
	Place getNearestPlace(string latitude, string longitude);
	int getPlacesCount();
	void addPlace(Place place);
private:
	InstancePlaces();
	void loadPlaces(string path);
	static InstancePlaces* single;
	static bool instanceFlag;
	vector<Place> places;
};

#endif /* SRC_CONF_INSTANCEPLACES_H_ */
