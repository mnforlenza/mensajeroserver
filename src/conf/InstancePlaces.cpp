/*
 * InstancePlaces.cpp
 *
 *  Created on: 14/6/2015
 *      Author: marcos
 */

#include "InstancePlaces.h"

bool InstancePlaces::instanceFlag = false;
InstancePlaces* InstancePlaces::single = NULL;

InstancePlaces::InstancePlaces() {
	loadPlaces(PLACES_CONF_PATH);
}

InstancePlaces::~InstancePlaces() {
	// TODO Auto-generated destructor stub
}

/**
 * Carga lugares
 * @param path path al archivo de lugares
 */
void InstancePlaces::loadPlaces(string path) {
	string line;
	string json = "";
	ifstream myfile(path);
	if (myfile.is_open()) {
		while (getline(myfile, line)) {
			json += line;
		}
		myfile.close();
	}

	Json::Value root;
	Json::Reader reader;
	bool parsedSuccess = reader.parse(json, root, false);

	const Json::Value array = root["places"];
	if (!array.empty()) {
		for (unsigned int index = 0; index < array.size(); ++index) {
			const Json::Value innerRoot = array[index];
			string latitude = innerRoot["latitude"].asString();
			string longitude = innerRoot["longitude"].asString();
			string description = innerRoot["description"].asString();

			places.push_back(Place(latitude,longitude,description));
		}

	}
}

/**
 * Devuelve el objeto Singleton
 * @return el objeto Singleton
 */
InstancePlaces* InstancePlaces::getInstance(){
	if(!instanceFlag){
		single = new InstancePlaces();
        instanceFlag = true;
	}
	return single;
}

/**
 * Devuelve el lugar más cercano dada una latitud y longitud
 * @param latitude latitud
 * @param longitude longitud
 * @return el lugar más cercano
 */
Place InstancePlaces::getNearestPlace(string latitude, string longitude){
	Place min = places[0];
	double minDistance = 0;
	Place aux = Place(latitude, longitude, "");
	for (unsigned int index = 0; index < places.size(); ++index) {
		Place actual = places[index];
		double distance = actual.calculateDistance(aux);
		if(index == 0){
			minDistance = distance;
		}else{
			if(distance < minDistance){
				minDistance = distance;
				min = places[index];
			}
		}
	}
	return min;
}

/**
 * Cuenta la cantidad de lugares
 * @return la cantidad de lugares
 */
int InstancePlaces::getPlacesCount(){
	return places.size();
}

/**
 * Agrega un lugar
 * @param place el lugar a agregar
 */
void InstancePlaces::addPlace(Place place){
	places.push_back(place);
}
