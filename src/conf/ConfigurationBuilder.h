/*
 * ConfigurationBuilder.h
 *
 *  Created on: 27/3/2015
 *      Author: marcos
 */

#ifndef CONFIGURATIONBUILDER_H_
#define CONFIGURATIONBUILDER_H_

#ifndef CONFIGURATION_H_
#include "Configuration.h"
#endif

#include <iostream>
#include <fstream>
#include "json/json.h"

using namespace std;

#define SERVER_PORT "server.port"
#define LOGGER_LEVEL "logger.level"
#define LOGGER_NAME "logger.name"
#define TEST_ENABLED "test.enabled"
#define TEST_DB_DIRECTORY "test.db.directory"
#define PROD_DB_DIRECTORY "prod.db.directory"

/**
 * Clase que se encarga de contruir por partes un archivo de configuración
 */
class ConfigurationBuilder {
public:
	ConfigurationBuilder(string configurationFilePath);
	~ConfigurationBuilder();
	Configuration build();
private:
	string json;
};

#endif /* CONFIGURATIONBUILDER_H_ */
