#ifndef _LOGGERLEVEL_H
#define _LOGGERLEVEL_H

/**
 * Enumerado que representa los distintos niveles de log
 */
enum LogLevel { DEBUG = 0, INFO = 1, WARN = 2, ERROR=3, FATAL=4 };

#endif
