#ifndef _LOGGER_H
#define _LOGGER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <time.h>
#include "LogLevel.h"
#include "Configuration.h"
#include "ConfigurationBuilder.h"
#include "ConfigurationException.h"
#include "ConfigurationException.h"

using namespace std;

#define NIVEL_GLOBAL LogLevel::DEBUG

/**
 * Clase utilitaria para loguear
 */
class Logger {

public :
	
private:
	Logger(const string &clazz);
	string clazz;
	static ofstream logFile;
	bool appendToConsole;
	static LogLevel logLevel;
	int instances;
	static vector<string> instancesInUse;
	int getQuantityInUse();
public:
	static Logger* getLogger(const string &clazz);
	void fatal(const string&);
	void error(const string&);
	void warn(const string&);
	void debug(const string&);
	void info(const string&);
	void log(const string&);
	void log(const string&, LogLevel);
	
	void setLogLevel(LogLevel);
	
	void setAppendToConsole(const bool&);
	bool getAppendToConsole();

	void logInstancesInUse();

	~Logger();
	void close();
};

#endif
