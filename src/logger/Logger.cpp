#include "Logger.h"

using namespace std;

vector<string> Logger::instancesInUse;
ofstream Logger::logFile;
LogLevel Logger::logLevel = DEBUG;

Logger::Logger(const string &clazz) {
	this->clazz = clazz;
	this->appendToConsole = true;

	if (getQuantityInUse() == 0) {
		ConfigurationBuilder builder("./conf/conf.json");
		Configuration result = builder.build();
		string loggerLevel = result.getLoggerLevel();
		string fileName = result.getLoggerName();
		if (loggerLevel.compare("DEBUG")) {
			Logger::logLevel = LogLevel::DEBUG;
		} else if (loggerLevel.compare("INFO")) {
			Logger::logLevel = LogLevel::INFO;
		} else if (loggerLevel.compare("WARN")) {
			Logger::logLevel = LogLevel::WARN;
		} else if (loggerLevel.compare("ERROR")) {
			Logger::logLevel = LogLevel::ERROR;
		} else if (loggerLevel.compare("FATAL")) {
			Logger::logLevel = LogLevel::FATAL;
		} else {
			throw ConfigurationException();
		}
		Logger::logFile.open(fileName);
		if(Logger::logFile.bad()){
			throw new ConfigurationException();
		}
	}

	this->instances = getQuantityInUse();
	vector<string>::iterator it = Logger::instancesInUse.begin();
	it += this->instances;

	instancesInUse.insert(it, clazz);
}

Logger::~Logger() {

	if (getQuantityInUse() == 0) {
		debug("Cerrando el archivo de log y destruyendo la ultima instancia.");
		//if (this->logFile != NULL) {
			this->logFile.close();
		//}
	}
	Logger::instancesInUse[this->instances] = "";
}

/**
 * Obtiene la cantidad de loggers en uso
 * @return cantidad de loggers en uso
 */
int Logger::getQuantityInUse() {
	int cantidad = 0;
	for (int i = 0; i < Logger::instancesInUse.size(); i++) {
		if (Logger::instancesInUse.at(i) != "") {
			cantidad++;
		}
	}
	return cantidad;
}

/**
 * Obtiene el logger para esa clase particular
 * @param clazz clase donde se logueará
 * @return el logger para esa clase particular
 */
Logger *Logger::getLogger(const string &clazz) {
	return new Logger(clazz);
}

/**
 * Log en nivel warn
 * @param mensaje el mensaje a loguear
 */
void Logger::warn(const string& mensaje) {
	log(mensaje, LogLevel::WARN);
}

/**
 * Log en nivel error
 * @param mensaje el mensaje a loguear
 */
void Logger::error(const string& mensaje) {
	log(mensaje, LogLevel::ERROR);
}

/**
 * Log en nivel debug
 * @param mensaje el mensaje a loguear
 */
void Logger::debug(const string& mensaje) {
	log(mensaje, LogLevel::DEBUG);
}

/**
 * Log en nivel info
 * @param mensaje el mensaje a loguear
 */
void Logger::info(const string& mensaje) {
	log(mensaje, LogLevel::INFO);
}

/**
 * Log genérico
 * @param mensaje el mensaje a loguear
 */
void Logger::log(const string& mensaje) {
	log(mensaje, LogLevel::INFO);
}

/**
 * Log en nivel fatal
 * @param mensaje el mensaje a loguear
 */
void Logger::fatal(const string& mensaje) {
	log(mensaje, LogLevel::FATAL);
}

/**
 * Log en nivel pasado por parámetro
 * @param mensaje el mensaje a loguear
 * @param nivel el nivel de log
 */
void Logger::log(const string& mensaje, LogLevel nivel) {

	if (nivel < this->logLevel || this->logLevel < NIVEL_GLOBAL) {
		return;
	}

	struct tm *p_local_t;
	time_t t;

	t = time(NULL);
	p_local_t = localtime(&t);

	string sNivel("[INFO]\t");

	if(nivel == FATAL){
		sNivel = "[FATAL]\t";
	}else if(nivel == ERROR){
		sNivel = "[ERROR]\t";
	}else if(nivel == WARN){
		sNivel = "[WARN]\t";
	}else if(nivel == INFO){
		sNivel = "[INFO]\t";
	}else if(nivel == DEBUG){
		sNivel = "[DEBUG]\t";
	}
	//FORMATO: [NIVEL] yyyy-M-d HH:mm:ss mensaje
	logFile << (1900 + p_local_t->tm_year) << "-" << p_local_t->tm_mon
	<< "-" << p_local_t->tm_mday << " " << p_local_t->tm_hour << ":"
	<< p_local_t->tm_min << ":" << p_local_t->tm_sec << " " << sNivel << " " << this->clazz << " " << mensaje
	<< endl;

	//TODO REFACTORIZAR PARA NO TENER QUE REPETIR ESTE CODIGO
	if(appendToConsole == true) {

		cout << (1900 + p_local_t->tm_year) << "-" << p_local_t->tm_mon
		<< "-" << p_local_t->tm_mday << " " << p_local_t->tm_hour << ":"
		<< p_local_t->tm_min << ":" << p_local_t->tm_sec << " " << sNivel << " " << this->clazz << " " << mensaje
		<< endl;
	}

}

void Logger::close() {
	//if (this->logFile != NULL) {
		this->logFile.close();
	//}
}
