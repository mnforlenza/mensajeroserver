/*
 * ConfigurationException.h
 *
 *  Created on: 25/4/2015
 *      Author: marcos
 */

#ifndef SRC_EXCEPTION_CONFIGURATIONEXCEPTION_H_
#define SRC_EXCEPTION_CONFIGURATIONEXCEPTION_H_

#include "Exception.h"

/**
 * Clase que representa una excepción de configuración
 */
class ConfigurationException : public Exception {
public:
	ConfigurationException();
	virtual ~ConfigurationException();
};

#endif /* SRC_EXCEPTION_CONFIGURATIONEXCEPTION_H_ */
