/*
 * UserNotFoundException.cpp
 *
 *  Created on: 18/4/2015
 *      Author: marcos
 */

#include "UserNotFoundException.h"

UserNotFoundException::UserNotFoundException() : Exception::Exception(USER_NOT_FOUND_MESSAGE) {

}

UserNotFoundException::~UserNotFoundException() {
	// TODO Auto-generated destructor stub
}

