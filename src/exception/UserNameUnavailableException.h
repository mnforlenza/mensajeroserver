/*
 * UserNameUnavailableException.h
 *
 *  Created on: 21/4/2015
 *      Author: marcos
 */

#ifndef SRC_EXCEPTION_USERNAMEUNAVAILABLEEXCEPTION_H_
#define SRC_EXCEPTION_USERNAMEUNAVAILABLEEXCEPTION_H_

#include "Exception.h"
#include <iostream>

using namespace std;

/**
 * Clase que representa una excepción por nombre de usuario ocupado
 */
class UserNameUnavailableException : public Exception{
public:
	UserNameUnavailableException();
	virtual ~UserNameUnavailableException();
};

#endif /* SRC_EXCEPTION_USERNAMEUNAVAILABLEEXCEPTION_H_ */
