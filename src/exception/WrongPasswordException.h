/*
 * WrongPasswordException.h
 *
 *  Created on: 19/4/2015
 *      Author: marcos
 */

#ifndef SRC_EXCEPTION_WRONGPASSWORDEXCEPTION_H_
#define SRC_EXCEPTION_WRONGPASSWORDEXCEPTION_H_

#include "Exception.h"
#include <iostream>

using namespace std;

/**
 * Clase que representa una excepción de password incorrecto
 */
class WrongPasswordException : public Exception {
public:
	WrongPasswordException();
	virtual ~WrongPasswordException();
};

#endif /* SRC_EXCEPTION_WRONGPASSWORDEXCEPTION_H_ */
