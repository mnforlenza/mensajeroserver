/*
 * UserNotFoundException.h
 *
 *  Created on: 18/4/2015
 *      Author: marcos
 */

#ifndef SRC_EXCEPTION_USERNOTFOUNDEXCEPTION_H_
#define SRC_EXCEPTION_USERNOTFOUNDEXCEPTION_H_

#include "Exception.h"
#include <iostream>

using namespace std;

/**
 * Clase que representa una excepción de usuario no encontrado
 */
class UserNotFoundException : public Exception {
public:
	UserNotFoundException();
	virtual ~UserNotFoundException();
};

#endif /* SRC_EXCEPTION_USERNOTFOUNDEXCEPTION_H_ */
