/*
 * Exception.h
 *
 *  Created on: 18/4/2015
 *      Author: marcos
 */

#ifndef SRC_EXCEPTION_EXCEPTION_H_
#define SRC_EXCEPTION_EXCEPTION_H_

#define WRONG_PASSWORD_MESSAGE "Contraseña incorrecta"
#define USER_NOT_FOUND_MESSAGE "Usuario no encontrado"
#define USER_NAME_UNAVAILABLE "Nombre de usuario en uso"
#define CONFIGURATION_ERROR "Error al cargar la configuracion"

#include <iostream>

using namespace std;

/**
 * Clase que representa una excepción genérica
 */
class Exception {
public:
	Exception();
	Exception(string message);
	string getMessage();
	virtual ~Exception();
private:
	string message;
};

#endif /* SRC_EXCEPTION_EXCEPTION_H_ */
