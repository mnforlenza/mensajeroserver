import requests
import unittest
import json

class TestUserServices(unittest.TestCase):
	def __init__(self, *args, **kwargs):
		super(TestUserServices, self).__init__(*args, **kwargs)
		self.__api_base_url = "http://localhost:8000"
		self.__base_url = "/api"
		self.__user_url = "/user"
		
	def test_user_create(self):
		request_body = { 'userName' : 'gino', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
		
	def test_user_create_failed(self):
		request_body = { 'userName' : 'gino13', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"false")
		
	def test_user_create(self):
		request_body = { 'userName' : 'gino', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
		
	def test_user_login(self):
		request_body = { "userName" : "gino2" , "password" : "pass"}
		user = '/login'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
		
	def test_user_login_shoud_work(self):
		request_body = { 'userName' : 'gino', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		request_body2 = { "userName" : "gino" , "password" : "pass"}
		user2 = '/login'
		r2 = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user2, data=json.dumps(request_body2))
		data_string = json.dumps(r2.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"true")
	
	def test_user_logout(self):
		request_body = { "userName" : "gino2" }
		user = '/logout'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
		
	def test_user_login_shoud_work(self):
		request_body = { 'userName' : 'gino21312'}
		user = '/logout'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"false")
	
	def test_user_get(self):
		request_body = { "userName" : "gino2" }
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url, params = request_body)
		self.assertEqual(r.status_code, 200)
		
	def test_user_get_should_work(self):
		request_body = { 'userName' : 'gino4', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		request_body = { "userName" : "gino4" }
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url, params = request_body)
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"true")
		
	def test_user_online(self):
		request_body = { "userName" : "gino2" }
		user = '/online'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params = request_body)
		self.assertEqual(r.status_code, 200)
	
	def test_user_online_should_failed(self):
		request_body = { "userName" : "gfsfsdfsfino2" }
		user = '/online'
		r = requests.get(self.__api_base_url + self.__base_url + self.__user_url + user, params = request_body)
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"false")
	
	def test_user_update(self):
		request_body = { 'userName' : 'gino', 'password': 'newpass', 'email' : 'email@email.com', 'photo': 'newphoto' }
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
	
	def test_user_update_should_work(self):
		request_body = { 'userName' : 'gino', 'password': 'newpass', 'email' : 'email@email.com', 'photo': 'newphoto' }
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"true")
	
	def test_user_update_should_failed(self):
		request_body = { 'userName' : 'asdasdgindaso', 'password': 'newpass', 'email' : 'email@email.com', 'photo': 'newphoto' }
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"false")
	
	def test_user_checkin(self):
		request_body = { 'userName' : 'gino', 'password': 'pass', 'email' : 'email@email.com' }
		user = '/create'
		r = requests.post(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		request_body = { 'userName' : 'gino', 'latitude': '-34.545143', 'longitude' : '-58.449840'}
		user = '/checkin'
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		self.assertEqual(r.status_code, 200)
	
	def test_user_checkin_should_work(self):
		request_body = { 'userName' : 'gino', 'latitude': '-34.545143', 'longitude' : '-58.449840'}
		user = '/checkin'
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"true")
	
	def test_user_checkin_should_failed(self):
		request_body = { 'userName' : 'sdasdsda', 'latitude': '-34.545143', 'longitude' : '-58.449840'}
		user = '/checkin'
		r = requests.put(self.__api_base_url + self.__base_url + self.__user_url + user, data=json.dumps(request_body))
		data_string = json.dumps(r.json())
		decoded = json.loads(data_string)
		self.assertEqual(decoded['succes'],"false")
	
		
	
