#!/bin/bash
lcov --capture --directory . --output-file coverage.info
if [ $? = 0 ]; then
	lcov --directory . --output-file coverage.info --remove coverage.info "/libs/*" "/usr/*" "/tests/*" "api/*"
	genhtml coverage.info --output-directory out
else
	echo "Error on lcov captur"
	exit -1
fi
