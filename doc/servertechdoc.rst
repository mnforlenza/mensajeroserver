.. _servertechdoc:

Documentación técnica (servidor)
===============================

.. toctree::
   :maxdepth: 2

**Diseño inicial**
-------------------------------

El diseño inicial fue concebido como una aplicación rest regular con 4 componentes importantes.

* Servicios: Son lo encargados de resolver la lógica de negocio, pueden o no delegar tareas en un DAO
* DAOs: Son los encargados de acceder a la base de datos.
* DTOs: Son objetos de transporte sin comportamiento.
* API: Es la encargada de exponer los servicios rest a través de ciertos métodos HTTP.

.. image:: design.png

**Implementación**
-------------------------------

A través del diseño inicial se detallo un poco más cada capa y se agrego una capa de modelo donde estarían las representaciones de las entidades del negocio.
A continuación se muestra un diagrama de clases simplificado, tanto en notación como en contenido, para poder tener un pantallazo del diseño general. Si bien es simplificado se decidio incluir a todos los objetos.

.. image:: clases.png



**Documentación Fuentes**
-------------------------------
Para documentar  los fuentes utilizamos la herramienta Doxygen. En el siguiente link se puede encontrar toda la documentación:`doxy`_

.. _doxy: ../../../doxydir/html/index.html


**Testing Server**
-------------------------------

Dentro de la directorio /tests se encuentran todos los tests relacionados al servidor. Se utilizó la herramienta GTest.
Se definieron tests simples y no se utilizan funcionalidades avanzadas del framework de testing. Simplemente se utilizan EXPECT_EQ (método para comparar un resultado obtenido con uno esperado) y la integración con CMake.
En total se realizaron 37 tests y antes de cada release se chequea que den correctos.

.. image:: testing1.png





**Code Coverage**
-------------------------------

Para el code coverage se utiliza la herramienta gcov.
Se decidieron excluir ciertos directorios ya que no tenía sentido para la corrida. Todas las librerías externas, ya que poseen el código fuente dentro del proyecto y tiene sentido que no se usen en su totalidad fueron quitadas del análisis.
También se decidió quitar del code coverage, el directorio api ya que será testeado con otra herramienta (se explicara en la siguiente sección).
Otro directorio que se excluye es el de tests ya que, obviamente, todos los fuentes de test son cubiertos al ejecutarse los tests. Esto sube el porcentaje pero es incorrecto incluirlo.
Para realizar el code coverage se generó un script: “coverage.sh”

.. image:: scriptcoverage.png

Los resultados son menores al 90% pero muy por encima del nivel requerido de 75%.

.. image:: coverage.png


**Testing REST API**
-------------------------------
Para testear la API rest se utilizó un script en python “test_rest_api” con los siguientes paquetes: requests, unittest y json.

.. image:: testrestapi.png

**Documentación REST API**
-------------------------------
Para documentar la api rest generamos un archivo html (`rest-api-doc`_) con el detalle de todos los servicios.

.. _rest-api-doc: ../../../src/api/doc/rest-api-doc.html
