.. _proyect:

Manual de proyecto (gestión y división de tareas)
===============================

.. toctree::
   :maxdepth: 2

**Descripción de la gestión y división de tareas realizadas**
-------------------------------
En el presente documento se detallara como se gestionaron y dividieron las tareas del proyecto: “Mensajero”.  Comentaremos las herramientas adoptadas y la modalidad de trabajo que aplicamos.

**Herramientas: su uso**
-------------------------------
Las herramientas que utilizamos para la gestión y división de tareas realizadas son:

.. image:: herramientas.png


**Gestión del proyecto**
-------------------------------

Gestión de la demanda

* Extrajimos los requerimientos del enunciado del trabajo práctico.
	https://docs.google.com/document/d/12Dt5EfeKR3LRvcGKp_kKbeTmdU6fIRaZMeOiLjXsIQc/edit#

* Registramos los requerimientos en cards de Trello,
	https://trello.com/b/3CA6FW42  

* Refinamos los requerimientos en las sucesivas clases con el ayudante. Presencialmente y por correo electrónico. DRIVE con la documentación:
	https://drive.google.com/drive/u/0/my-drive?usp=docs_home 


**División de las tareas**
-------------------------------

Al inicio del proyecto todos trabajamos en la investigación de las herramientas nuevas, la BBDD, el WebServer, en la configuración de los entornos de desarrollo.  A partir de ahí y basados en las áreas de conocimiento de cada uno de los integrantes y teniendo en cuenta la naturaleza del proyecto dividimos las tareas en los siguientes grupos:  

.. image:: division_tareas.png

Trabajamos como grupo en las definiciones y en la toma de decisiones de diseño, arquitectura y de la funcionalidad.  Si bien todos estuvimos presentes en los desarrollos de todos los componentes tratamos que para cada área de interés hubiera al menos 2 personas siempre con mayor grado de conocimiento.  
Eso lo expresamos en las siguientes asignaciones:

* Servicios REST → Marcos Forlenza (MF), Gonzalo Velasco (GV)
* BBDD RocksDB → Marcos Forlenza (MF)
* App Android → Jimena Tapia (JT), Laura Rodriguez (LR)
* API → Jimena Tapia (JT), Gonzalo Velasco (GV)
* Documentación → Laura Rodriguez (LR)


**CheckPoints**
-------------------------------

Para los checkpoints del 30/04 y del 04/06 generamos un informe a modo de resumen para entregar al ayudante.  Los mismos pueden verse en los siguientes links:

* CHKP1→ https://drive.google.com/open?id=0B0G2NRpqoVNTYU83SUFjVDNncXM&authuser=0 
* CHKP2→ https://drive.google.com/open?id=0B0G2NRpqoVNTWG5KXzNsWTN1Skk&authuser=0 

Asimismo, generamos para esas mismas fechas un branch en cada uno de los proyectos (cliente y server).

Branches del Server: 

* https://bitbucket.org/mnforlenza/mensajeroserver/branch/checkpoint1
* https://bitbucket.org/mnforlenza/mensajeroserver/branch/checkpoint2

Branches del Cliente:

* https://bitbucket.org/mnforlenza/mensajeroclient/branch/checkpoint1
* https://bitbucket.org/mnforlenza/mensajeroclient/branch/checkpoint2


