.. _changelog:

Changelog
===============================

.. toctree::
   :maxdepth: 2

Para el seguimiento de tareas usamos Trello. Generamos grupos de tareas para el cliente, Server y tareas generales para tener a simplevista el estado de situación del proyecto

Para ver las funcionalidades que entraron en la release bastara con ver las tareas bajo las tarjetas
* Client Done
* Server Done
* Done (tareas generales)

El link al proyecto es el siguiente: https://trello.com/b/3CA6FW42/fiubataller012015
