.. _clienttechdoc:

Documentación técnica (cliente)
===============================

.. toctree::
   :maxdepth: 2

**Diagrama de paquetes**
-------------------------------

.. image:: Diagrama_Cliente.png


Descripción de clases

* SessionManager: Clase que contiene los datos del usuario y mantiene la sesión mientras este está logueado a la aplicación.
* ApiService: Clase que funciona como interfaz con el servidor, contiene todas las llamadas a la api.
* MessageService: Servicio de pooling para obtener mensajes del servidor cada determinado tiempo.
* RestServiceAsync: Servicio de comunicación con el servidor, funciona de manera asincrónico.
* Actividades:Una aplicación que tiene una UI visible se implementa con una actividad. Cuando un usuario selecciona una aplicación desde la pantalla de inicio o el iniciador de aplicación, se inicia una actividad.
* Fragmentos:Un fragmento es una sección “modular” de interfaz de usuario embebida dentro de una actividad anfitriona, el cual permite versatilidad y optimización de diseño. Se trata de mini actividades contenidas dentro de una actividad anfitriona, manejando su propio diseño (un recurso layout propio) y ciclo de vida.


**Diagrama de interacción entre clases**
-------------------------------

.. image:: Diagrama_Interaccion.png


**Diagrama de ejemplo interacción para la clase User**
-------------------------------

.. image:: Diagrama_Ejemplo_Interaccion.png

**Diseño de la Interfaz aplicación**
-------------------------------

UI Material design:
Se eligió utilizar material design por su propuesta de diseño simple, facilitando la usabilidad de la aplicación.


**Librerías utilizadas**
-------------------------------

Comunicación entre activities/fragments y cliente

* com.squareup.retrofit:retrofit:1.9.0
* com.squareup:otto:1.3.7

Inputs y botones:

* com.rengwuxian.materialedittext:library:2.0.3 
* com.github.navasmdc:MaterialDesign:1.+@aar
* it.neokree:MaterialTabs:0.11
* net.yanzm:mth:1.0.1


Dialogos con mensajes

* cn.pedant.sweetalert:library:1.3

Floating action buttons menu

* com.getbase:floatingactionbutton:1.9.0

GoogleMaps

* com.google.android.gms:play-services:6.5.87

Cards para mensajes

* com.github.dexafree:materiallist:2.4.3
