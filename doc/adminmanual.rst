.. _adminmanual:

Manual del Administrador
===============================

.. toctree::
   :maxdepth: 2
   
|
|
|
|

**Configuración del servidor**
-------------------------------

Para poder configurar el servidor se utilizan dos archivos json:

**conf.json:** en él se configura el puerto del servidor, el nivel del log, el nombre del archivo de log, si el entorno es de testing y los directorios donde se alojarán las bases de datos de testing y de produccion.
| Ej:

 {
     "server.port" : "8000",
     "logger.level" : "DEBUG",
     "logger.name" : "logger.txt",
     "test.enabled" : "true",
     "test.db.directory" : "/tmp/testdb",
     "prod.db.directory" : "/tmp/proddb"
 }

**places.json:** en él se configurarán las ubicaciones disponibles para hacer checkin.
| Ej:
 {
     "places" : [{"latitude": "-34.617525", "longitude": "-58.368285", "description" : "Facultad Ingenieria UBA, Paseo Colón"},
     {"latitude": "-34.588353", "longitude": "-58.396256", "description" : "Facultad Ingenieria UBA, Las Heras"},
     {"latitude": "-34.541127", "longitude": "-58.445918", "description" : "Ciudad Universitaria UBA"}]
 }
 
| Cabe aclarar que existe un tercer archivo, “controllerfactory.json” que se utiliza para configurar la rest api. Está destinado a los desarrolladores que quieran extender la api y no se pretende explicarlo en esta sección de configuración.
