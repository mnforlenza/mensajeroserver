.. doc documentation master file, created by
   sphinx-quickstart on Sat May 23 12:00:40 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

"Que onda FIUBA"
===============================

Dentro de esta página se encontrará toda la documentación del desarrollo de la aplicación "Que onda FIUBA"

.. toctree::
   :maxdepth: 2



Índice
==================

* :ref:`changelog`
* :ref:`readme`
* :ref:`adminmanual`
* :ref:`servertechdoc`
* :ref:`clienttechdoc`
* :ref:`manual`
* :ref:`proyect`


