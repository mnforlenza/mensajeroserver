.. _readme:

README/INSTALL
===============================

.. toctree::
   :maxdepth: 2

"Que onda FIUBA" es una aplicación de mensajería instantánea que consta de un cliente en Android y un Servidor en c++.

|
|
|
|

**Sistema operativo**
------------------------------
* *Server*: GNU/Linux Debian/Ubuntu x64
* *Cliente*: Android 4.3/4.4



**Dependencias Servidor**
------------------------------

* Tener instalado: cmake, snappy, bzip2 y zlib ( *sudo apt-get install libgflags-dev zlib1g-dev libbz2-dev cmake build-essential*)


**Dependencias Cliente**
------------------------------

* Tener instalado: Android Studio, sdk, Genymotion (opcional)
 https://developer.android.com/sdk/index.html
 https://www.genymotion.com/#!/download
  
**Instalación Servidor**
------------------------------
1. Descargar el código desde https://bitbucket.org/mnforlenza/mensajeroserver el branch es 'rc1.0'
2. Desde la consola, en el directorio donde se bajó el proyecto, posicionarse en el subdirectorio libs/jsoncpp y ejecutar 
 * cmake .
 * make
3. Desde la consola, en el directorio donde se bajó el proyecto, posicionarse en el directorio libs/rocksdb y ejecutar
 * make static_lib
 * make shared_lib
4. Desde la consola, en el directorio donde se bajó el proyecto, posicionarse en el directorio root (mensajeroserver) y ejecutar
 * cmake . (para ejecutar los tests correr cmake -Dtest=ON .)
 * make
 
Esto genera dos ejecutables, uno llamado **main** (que genera el server y lo levanta en el puerto configurado por el archivo conf/conf.json) y otro **runUnitTests** (que corre todos los test unitarios). 

**Instalación Cliente**
------------------------------
1. Descargar el código desde https://bitbucket.org/mnforlenza/mensajeroclient  el branch es 'rc1.0'.
2. Configurar en la clase UrlEndpoints la dirección donde se configurará el servidor
 * Ejemplo: *URL_API = "http://192.168.56.1:8000";*
3. Ejecutar a través de Android Studio (o mismo desde la línea de comando) la tarea de gradle llamada **assembleDebug**
4. Se generará la app en la ruta './build/outputs/apk/app-debug.apk'
5. Instalarla con cualquier instalador de paquetes en un dispositivo móvil




 



